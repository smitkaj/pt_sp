/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.graph;

import java.io.File;
import java.io.IOException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Jan Smitka <jan@smitka.org>
 */
public class GraphLoaderTest {
	private static final short WORLD_SIZE = 1000;
	private static final short DENSITY = 500;
	private static final short SIZE = 3001;
	
	private static final String DATA_FILE = "graph.bin";
	private static final String TEMP_FILE = "temp.graph";
	
	public GraphLoaderTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}
	
	@Before
	public void setUp() {
	}

	@Test
	public void testPerformance() throws GraphLoaderException {		
		long start = System.currentTimeMillis();
		
		try {
			Graph graph = GraphLoader.load(DATA_FILE);
			GraphLoader.save(graph, TEMP_FILE);
		} catch (IOException e) {
			fail(e.getMessage());
		}
		
		
		long total = System.currentTimeMillis() - start;
		System.out.println(total);
	}
}
