/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.graph;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Jan Smitka <jan@smitka.org>
 */
public class GraphGeneratorTest {
	
	public GraphGeneratorTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}
	
	@Before
	public void setUp() {
	}

	@Test
	public void testGenerateGraph() {
		GraphGenerator generator = new GraphGenerator((short)1000, (short)500);
		Graph graph = generator.generateGraph((short)3000);
		
		for (GraphNode node : graph.getNodes()) {
			assertTrue(node.getNumbOfNeighb() <= 500);
		}
	}
}
