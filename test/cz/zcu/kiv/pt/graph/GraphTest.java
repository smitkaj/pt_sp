/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.graph;

import java.util.Queue;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Jan Smitka <jan@smitka.org>
 */
public class GraphTest {
	private Graph graph;
	
	public GraphTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}
	
	@Before
	public void setUp() {
		graph = new Graph((short)9, (short)100);
		for (short i = 0; i < 9; i++) {
			graph.addNode(new GraphNode(i));
		}
		graph.setDistance(0, 3, (short) 2);
		graph.setDistance(3, 6, (short) 8);
		graph.setDistance(6, 7, (short) 3);
		graph.setDistance(7, 8, (short) 2);
		graph.setDistance(4, 7, (short) 2);
		graph.setDistance(3, 4, (short) 2);
		graph.setDistance(4, 5, (short) 1);
		graph.setDistance(5, 1, (short) 3);
		graph.setDistance(5, 2, (short) 1);
		graph.setDistance(1, 2, (short) 1);
	}

	@Test
	public void testFloydWarshall() {
		graph.floydWarshall();
		assertPathEquals(graph.getPath(0, 8), 0, 3, 4, 7, 8);	
		assertPathEquals(graph.getPath(8, 0), 8, 7, 4, 3, 0);
		assertPathEquals(graph.getPath(0, 1), 0, 3, 4, 5, 2, 1);
		assertPathEquals(graph.getPath(1, 0), 1, 2, 5, 4, 3, 0);
		assertPathEquals(graph.getPath(3, 6), 3, 4, 7, 6);
		assertPathEquals(graph.getPath(6, 3), 6, 7, 4, 3);
	}
	
	
	private void assertPathEquals(Queue<INode> actual, int... expected) {
		INode node;
		for (int i = 0; i < expected.length; i++) {
			node = actual.poll();
			assertEquals(expected[i], node.getId());
		}
		assertTrue(actual.isEmpty());
	}
}
