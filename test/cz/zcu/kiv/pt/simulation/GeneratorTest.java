package cz.zcu.kiv.pt.simulation;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Jan Smitka <jan@smitka.org>
 */
public class GeneratorTest {
    
    /** Počet iterací testů. */
    private final int ITERATIONS = 1000000;
    
    /** Instance generátoru. */
    private Generator generator;
    
    @Before
    public void setUp() {
        generator = new Generator();
    }
    
    @After
    public void tearDown() {
        generator = null;
    }

    /**
     * Test of exponentialDistribution method, of class Generator.
     */
    @Test
    public void testExponentialDistribution() {
        int mean = 1000;
        
        double sum = 0;
        int value;
        for (int i = 0; i < ITERATIONS; i++) {
            value = generator.exponentialDistribution(mean);
            sum += value;
        }
        
        double avg = sum / ITERATIONS;
        double eps = mean * (3000d / ITERATIONS);
        System.out.println("testExponentialDistribution: AVG = " + avg + "(expected: " + mean + " +- " + eps + ")");
        assertTrue("Average value " + avg + " differs from expected " + mean, (mean - eps) <= avg && avg <= (mean + eps));
    }

    /**
     * Test of uniformDistribution method, of class Generator.
     */
    @Test
    public void testUniformDistribution() {
        int min = 1;
        int max = 100;
        
        double sum = 0;
        int value;
        for (int i = 0; i < ITERATIONS; i++) {
            value = generator.uniformDistribution(min, max);
            assertTrue("Value " + value + " is not within the range of 1 - 1000.", 1 <= value && value <= 1000);
            sum += value;
        }
        
        double avg = sum / ITERATIONS;
        double expAvg = (min + max) / 2d;
        double eps = expAvg * (10000d / ITERATIONS);
        System.out.println("testUniformDistribution: AVG = " + avg + "(expected: " + expAvg + " +- " + eps + ")");
        assertTrue("Average value " + avg + " differs from expected " + expAvg, (expAvg - eps) <= avg && avg <= (expAvg + eps));
    }
}
