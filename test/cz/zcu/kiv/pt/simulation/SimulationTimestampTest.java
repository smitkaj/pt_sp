/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.simulation;

import org.junit.*;
import static org.junit.Assert.*;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.DAY;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.HOUR;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.MINUTE;
import java.util.Random;

/**
 *
 * @author Jan Smitka <jan@smitka.org>
 */
public class SimulationTimestampTest {
	
	public SimulationTimestampTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Test of toString method, of class SimulationTimestamp.
	 */
	@Test
	public void testToString() {
		SimulationTimestamp t = new SimulationTimestamp(0);
		assertEquals("1. den,  0:00:00", t.toString());
		t = new SimulationTimestamp(DAY / 2);
		assertEquals("1. den, 12:00:00", t.toString());
		t = new SimulationTimestamp(DAY);
		assertEquals("2. den,  0:00:00", t.toString());
		t = new SimulationTimestamp(2 * DAY + 8 * HOUR + 37 * MINUTE + 14);
		assertEquals("3. den,  8:37:14", t.toString());
		t = new SimulationTimestamp(3 * DAY + 18 * HOUR + 20 * MINUTE);
		assertEquals("4. den, 18:20:00", t.toString());
	}
	
	
	@Test
	public void testMin() {
		SimulationTimestamp t1 = new SimulationTimestamp(10);
		SimulationTimestamp t2 = new SimulationTimestamp(15);
		SimulationTimestamp t3 = new SimulationTimestamp(5);
		SimulationTimestamp t4 = new SimulationTimestamp(20);
		SimulationTimestamp t5 = new SimulationTimestamp(1);
		
		assertSame(t5, SimulationTimestamp.min(t1, t2, t3, t4, t5));
	}
}
