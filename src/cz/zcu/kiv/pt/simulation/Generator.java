package cz.zcu.kiv.pt.simulation;

import java.util.Random;

/**
 * Generátor náhodných hodnot.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class Generator {
    /** Generátor náhodných čísel. */
    private final Random random;
    
    /**
     * Vytvoří generátor.
     */
    public Generator() {
        random = new Random();
    }
    
    /**
     * Vygeneruje hodnotu dle exponenciálního rozdělení.
     * @param meanValue Střední hodnota.
     * @return Vygenerovaná hodnota.
     */
    public int exponentialDistribution(final int meanValue) {
        return (int) Math.ceil(-Math.log(1 - random.nextDouble()) * meanValue);
    }
    
    /**
     * Vygeneruje hodnotu v rovnoměrném rozdělení.
     * @param min Minimální hodnota.
     * @param max Maximální hodnota.
     * @return Vygenerovaná hodnota.
     */
    public int uniformDistribution(final int min, final int max) {
        return random.nextInt(max - min + 1) + min;
    }
}
