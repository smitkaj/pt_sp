package cz.zcu.kiv.pt.simulation;

import java.util.LinkedList;

/**
 * Výsledek plánování cesty mrazírenského vozu.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class SchedulerResult {
    /** Vypočítaná cena. */
    private final double cost;
    
    /** Naplánované akce. */
    private final LinkedList<RouteAction> actions;
    
    
    /**
     * Vytvoří výsledek s nekonečnou cenou - nesplnitelný.
     */
    public SchedulerResult() {
        cost = Double.POSITIVE_INFINITY;
        actions = null;
    }
    

    /**
     * Vytvoří výsledek se zadanou cenou a časovým rozvrhem akcí k vykonání.
     * @param routeCost Cena.
     * @param actionList Seznam akcí.
     */
    public SchedulerResult(final double routeCost, final LinkedList<RouteAction> actionList) {
        cost = routeCost;
        actions = actionList;
    }

    /**
     * Získá seznam akcí.
     * @return Seznam akcí k vykonání.
     */
    public LinkedList<RouteAction> getActions() {
        return actions;
    }

    /**
     * Získá vypočítanou cenu výjezdu.
     * @return Vypočítaná cena.
     */
    public double getCost() {
        return cost;
    }
    
    /**
     * Získá čas začátku výjezdu.
     * @return Začátek výjezdu.
     */
    public SimulationTimestamp getStartTime() {
        return actions.peek().getTime();
    }
}
