package cz.zcu.kiv.pt.simulation;

/**
 * Třída reprezentující výsledek operace zařazování objednávky do {@link Route}.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class RoutingResult {
    /** Cena. */
    private final double cost;
    
    /** Index, na který bude objednávka vložena. */
    private final int index;
    
    /** Výsledek plánovače. */
    private SchedulerResult result;
    
    
    /**
     * Zatím neznámý výsledek, tzn. s "nekonečnou" cenou.
     */
    public RoutingResult() {
        cost = Double.POSITIVE_INFINITY;
        index = 0;
    }
    
    /**
     * Vyvtoří výsledek routování.
     * @param routeCost Cena.
     * @param orderIndex Index.
     */
    public RoutingResult(final double routeCost, final int orderIndex) {
        this.cost = routeCost;
        this.index = orderIndex;
    }

    
    /**
     * Získá cenu plánované cesty.
     * @return Cena.
     */
    public double getCost() {
        if (result == null) {
            return cost;
        } else {
            return result.getCost();
        }
    }

    
    /**
     * Získá index, na který je vhodné objednávku vložit.
     * @return Index.
     */
    public int getIndex() {
        return index;
    }

    
    /**
     * Získá výsledek plánovače (pokud nějaký je).
     * @return Výsledek plánovače.
     */
    public SchedulerResult getResult() {
        return result;
    }

    /**
     * Nastaví výsledek plánovače.
     * @param schedulerResult Výsledek plánovače.
     */
    public void setResult(final SchedulerResult schedulerResult) {
        this.result = schedulerResult;
    }
}
