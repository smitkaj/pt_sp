package cz.zcu.kiv.pt.simulation;

import java.util.LinkedList;

/**
 * Třída zapouzdřující statistiky zmrzlinářství.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class NodeStatistics {
    /** ID pobočky. */
    private final short nodeId;

    /** Celkový počet objednaných kontejnerů. */
    private short cargoTotal = 0;

    /** Počet kontejnerů, které byly do zrmzlinářství doručeny. */
    private short cargoDelivered = 0;

    /** Seznam akcí aut, která ve zmrzlinářství vykládala. */
    private final LinkedList<UnloadingAction> unloadingActions;


    /**
     * Vytvoří prázdný kontejner na statistiky o daném zmrzlinářství.
     * @param nId ID pobočky.
     */
    public NodeStatistics(final short nId) {
        nodeId = nId;
        unloadingActions = new LinkedList<UnloadingAction>();
    }

    /**
     * Započítá objednávku zmrzlinářství.
     * @param order Objednávka, kterou zmrzlinářství vydalo. 
     */
    public void addOrder(final Order order) {
        cargoTotal += order.getCargo();
    }

    /**
     * Přidá výjezd do seznamu výjezdů, které vedou přes danou pobočku.
     * @param action Akce auta.
     */
    public void addUnloadingAction(final UnloadingAction action) {
        unloadingActions.add(action);
        cargoDelivered += action.getOrder().getCargo();
    }

    /**
     * Vrátí, kolik kontejnerů celkem bylo do zmrzlinářství doručeno.
     * @return Počet doručených kontejnerů.
     */
    public short getCargoDelivered() {
        return cargoDelivered;
    }


    /**
     * Vrátí, kolik kontejnerů celkem zmrzlinářství objednalo. Počítají se i nepřijaté objednávky.
     * @return Počet objednaných kontejnerů.
     */
    public short getCargoTotal() {
        return cargoTotal;
    }


    /**
     * Získá ID pobočky.
     * @return ID pobočky.
     */
    public short getNodeId() {
        return nodeId;
    }


    /**
     * Získá seznam vykládacích akcí, které náleží výjezdům, které ve zmrzlinářství vykládaly.
     * @return Seznam akcí vykládání.
     */
    public LinkedList<UnloadingAction> getUnloadingActions() {
        return unloadingActions;
    }
}
