package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.HOUR;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Plánovač rozvozů. Vytváří časový harmonogram jednotlivých výjezdů.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class Scheduler {
    /** Rezervovaná doba čekání na přeplánování výjezdu. */
    private static final int PLAN_TIME = 6 * HOUR;
    
    /**
     * Maximální náklad, který plánovač ještě ponechá pro naplánování.
     * Po jeho dosažení plánovač nebere ohled na rezervovaný čas plánování objednávky.
     */
    private static final int MAX_CARGO = Simulation.TRUCK_MAX_CARGO - (Math.round(Simulation.ORDER_MAX_CARGO / 2));
    
    /** Jak velké čekání u zmrzlinářství je plánovč ochoten připustit. */
    private static final int MAX_WAIT = 3 * HOUR;
    
    
    /** Objekt simulace. */
    private final Simulation simulation;
    
    
    
    /**
     * Připraví plánovač.
     * @param sim Objekt simulace.
     */
    public Scheduler(final Simulation sim) {
        this.simulation = sim;
    }
    
    /** Pomocná proměnná pro výpočet ceny. */
    private double currentCost;
    
    /** Pomocná proměnná pro uchování aktuálního času. */
    private SimulationTimestamp currentTime;
    
    /** Pomocná proměnná pro frontu akcí. */
    private LinkedList<RouteAction> actions;
    
    /** Právě zpracovávaný výjezd. */
    private Route currentRoute;
    
    /** Offset nejdelší cesty mezi 2 uzly. Použito pro zarovnávání v čase. */
    private int lTravelOffset;
    
    /** Offset prvního vykládání. */
    private int firstUnloading;
    
    
    
    /**
     * Zkusí naplánovat cestu pro výjezd mrazírenského vozu.
     * @param route Výjezd vozu.
     * @param orderTime Čas poslední přidané objednávky.
     * @return Výsledek plánování.
     */
    public SchedulerResult schedule(final Route route, final SimulationTimestamp orderTime) {
        lTravelOffset = 0;
        firstUnloading = 0;
        currentCost = 0;
        actions = new LinkedList<RouteAction>();
        this.currentRoute = route;
        
        SimulationTimestamp initialTime = route.getTimeCreated();
        // Zkusíme zarezervovat čas...
        if (route.getCargo() <= MAX_CARGO) {
            initialTime = initialTime.addTime(PLAN_TIME);
        }
        // ... a neplánujeme v minulosti.
        initialTime = SimulationTimestamp.max(orderTime, initialTime);
        
        INode mainNode = simulation.getMainNode();
        INode currentNode = mainNode;
        currentTime = initialTime;
        byte currentCargo = route.getCargo();
        
        TravelAction lastTravel = null;
        TravelAction longestTravel = null;
        UnloadingAction lastUnload = null;
        int longestTravelTime = 0;
        queueUnloading(mainNode, (byte) (-currentCargo));
        for (Order order : route.getOrders()) {
            if (currentNode != order.getNode()) {
                lastTravel = queueTravel(currentNode, order.getNode(), order.getCargo());
                if (lastTravel.getDuration() > longestTravelTime) {
                    longestTravel = lastTravel;
                }
                currentNode = order.getNode();
            }
            
            if (firstUnloading == 0) {
                firstUnloading = currentTime.getTime() - initialTime.getTime();
            }
            
            lastUnload = queueUnloading(order);
            currentCargo -= order.getCargo();
        }
        if (longestTravel.equals(lastTravel)) {
            longestTravel = null;
            longestTravelTime = 0;
        }
        // Připojíme cestu domů a finish!
        queueTravel(currentNode, mainNode, currentCargo);
        actions.add(new RouteFinish(route, currentTime, mainNode));
        
        // Kontrola, zda jsme nepřekročili čas na doručení poslední objednávky
        // Pokud ano, tak posuneme celý plán o zpět o rezervovaný plán, případně odmítneme objednávku.
        if (lastUnload.getTime().getTime() > simulation.getLastDeliveryTime() || actions.getLast().getTime().getTime() > simulation.getSimulationEnd()) {
            int diff = initialTime.diff(SimulationTimestamp.max(route.getTimeCreated(), orderTime));
            if (lastUnload.getTime().addTime(-diff).getTime() > simulation.getLastDeliveryTime()) {
                return new SchedulerResult();
            } else {
                initialTime = initialTime.addTime(-diff);
                shift(-diff);
            }
        }
        
        // Ověří, zda nejdelší plánovaná cesta může být použita pro zarovnávání podle 
        if (longestTravel != null && longestTravelTime <= (simulation.getNonUnloadTimespan() - MAX_WAIT)) {
            // Získá čas předchozího vykládání.
            lTravelOffset = actions.get(actions.indexOf(longestTravel) - 1).getTime().getTime() - initialTime.getTime();
        }
        
        currentTime = initialTime;
        NextTime nextTime;
        int delta;
        // Ověříme, zda je cesta dobře naplánovaná...
        while (!verify()) {
            // .. a případně jí posuneme v čase:
            nextTime = getNextTime();
            delta = nextTime.time.diff(currentTime);
            shift(delta);
            // Ověříme, zda zarovnáváme podle nejdelší cesty, která je kratší než délka "noci":
            if (nextTime.longestTravel && longestTravelTime < simulation.getNonUnloadTimespan()) {
                int index = actions.indexOf(longestTravel) + 1;
                delta = simulation.getNonUnloadTimespan() - longestTravelTime;
                queueWait(longestTravel.getEndNode(), delta, index);
                if (!verify()) {
                    actions.remove(index);
                    currentCost -= simulation.getWaitCost(delta);
                    shift(-delta, index);
                }
            }
            if (lastUnload.getTime().getTime() > simulation.getLastDeliveryTime()) {
                // cestu již nelze naplánovat
                return new SchedulerResult();
            }
        }
        
        if (actions.getLast().getTime().getTime() > simulation.getSimulationEnd()) {
            // Překročili jsme čas simulace - cestu nelze naplánovat
            return new SchedulerResult();
        }
        
        return new SchedulerResult(currentCost, actions);
    }
    
    
    
    /**
     * Zařadí do fronty akci vykládání/nakládání.
     * @param node Uzel.
     * @param cargo Náklad k vyložení či naložení.
     * @return Vytvořená akce.
     */
    private UnloadingAction queueUnloading(final INode node, final byte cargo) {
        UnloadingAction action = new UnloadingAction(currentRoute, currentTime, node, cargo, simulation.getUnloadingCost(cargo));
        actions.add(action);
        currentCost += action.getCost();
        currentTime = currentTime.addTime(simulation.getUnloadingTime(cargo));
        
        return action;
    }
    
    /**
     * Zařadí do fronty akci vykládání pro zadanou objednávku.
     * @param order Objednávka, se kterou je vykládání spojeno.
     * @return Vytvořená akce.
     */
    private UnloadingAction queueUnloading(final Order order) {
        UnloadingAction action = new UnloadingAction(currentRoute, currentTime, order, simulation.getUnloadingCost(order.getCargo()));
        actions.add(action);
        currentCost += action.getCost();
        currentTime = currentTime.addTime(simulation.getUnloadingTime(order.getCargo()));
        
        return action;
    }
    
    
    /**
     * Zařadí do fronty akci přesunu z uzlu do uzlu.
     * @param from Výchozí uzel.
     * @param to Koncový uzel.
     * @param cargo Náklad, který auto poveze (pro výpočet ceny).
     * @return Vytvořená akce.
     */
    private TravelAction queueTravel(final INode from, final INode to, final byte cargo) {
        TravelAction action = new TravelAction(currentRoute, currentTime, from, to, simulation.getDistance(from, to), simulation.getTravelTime(from, to), simulation.getTravelCost(from, to, cargo));
        actions.add(action);
        currentCost += action.getCost();
        currentTime = currentTime.addTime(action.getDuration());
        
        return action;
    }
    
    
    /**
     * Zařadí do fronty na určené místo akci čekání.
     * @param node Uzel, ve kterém čekat.
     * @param duration Délka čekání.
     * @param index Index ve frontě.
     * @return Vytvořená akce.
     */
    private WaitAction queueWait(final INode node, final int duration, final int index) {
        WaitAction action = new WaitAction(currentRoute, currentTime, node, duration, simulation.getWaitCost(duration));
        actions.add(index, action);
        currentCost += action.getCost();
        
        return action;
    }
    
    
    /**
     * Ověří, zda je výjezd platný, tedy že všechna vykládání probíhají v platný čas.
     * @return Ano/ne.
     */
    private boolean verify() {
        for (RouteAction action : actions) {
            if (action instanceof UnloadingAction && ((UnloadingAction) action).getCargo() > 0 && !simulation.timeIsUnloadable(action.getTime())) {
                return false;
            }
        }
        return true;
    }
    
    
    /**
     * Posune všechny akce o stanovený čas.
     * @param delta Čas posunutí.
     */
    private void shift(final int delta) {
        for (RouteAction action : actions) {
            action.setTime(action.getTime().addTime(delta));
        }
        currentTime = currentTime.addTime(delta);
    }
    
    /**
     * Posune všechny akce od určitého indexu o daný čas.
     * @param delta Čas posunutí.
     * @param index Počáteční index.
     */
    private void shift(final int delta, final int index) {
        ListIterator<RouteAction> itr = actions.listIterator(index);
        RouteAction action;
        while(itr.hasNext()) {
            action = itr.next();
            action.setTime(action.getTime().addTime(delta));
        }
    }
    
    
    /**
     * Získá další čas pro zarovnávání.
     * @return Další čas.
     */
    private NextTime getNextTime() {
        SimulationTimestamp firstUnloadTime;
        SimulationTimestamp longestTravelTime;
        
        firstUnloadTime = getNextUnloadStart(currentTime.addTime(firstUnloading)).addTime(-firstUnloading);
        if (lTravelOffset > 0) {
            longestTravelTime = getNextUnloadEnd(currentTime.addTime(lTravelOffset)).addTime(-lTravelOffset);
            if (longestTravelTime.compareTo(firstUnloadTime) < 0) {
                return new NextTime(longestTravelTime, true);
            }
        }
        return new NextTime(firstUnloadTime, false);
    }
    
    
    /**
     * Získá čas dalšího začátku vykládání.
     * @param time Čas, od kterého hledat.
     * @return Čas začátku vykládání.
     */
    private SimulationTimestamp getNextUnloadStart(final SimulationTimestamp time) {
        if (time.getSeconds() >= Simulation.UNLOADING_START) {
            return new SimulationTimestamp(time.getDay() + 1, Simulation.UNLOADING_START);
        } else {
            return new SimulationTimestamp(time.getDay(), Simulation.UNLOADING_START);
        }
    }
    
    
    /**
     * Získá čas dalšího konce vykládání.
     * @param time Čas, od kterého hledat.
     * @return Čas konce vykládání.
     */
    private SimulationTimestamp getNextUnloadEnd(final SimulationTimestamp time) {
        if (time.getSeconds() >= Simulation.UNLOADING_END) {
            return new SimulationTimestamp(time.getDay() + 1, Simulation.UNLOADING_END);
        } else {
            return new SimulationTimestamp(time.getDay(), Simulation.UNLOADING_END);
        }
    }
    
    
    /**
     * Pomocná třída nesoucí informace o průběhu zarovnávání.
     */
    private static class NextTime {
        /** Čas, na který zarovnat. */
        private final SimulationTimestamp time;
        
        /** Je zarovnáno podle nejdelší cesty? */
        private final boolean longestTravel;
        
        /**
         * Vytvoří obálku na informace o zarovnání.
         * @param timestamp Čas.
         * @param isLongestTravel Zarovnat podle nejdelší cesty?
         */
        public NextTime(final SimulationTimestamp timestamp, final boolean isLongestTravel) {
            this.time = timestamp;
            this.longestTravel = isLongestTravel;
        }

        /**
         * Jedná se o zarovnání podle nejdelší cesty?
         * @return {@code true}, pokud se jedná o zarovnání podle nejdelší cesty.
         */
        public boolean isLongestTravel() {
            return longestTravel;
        }

        /**
         * Čas zarovnání.
         * @return Čas zarovnání.
         */
        public SimulationTimestamp getTime() {
            return time;
        }
    }
}
