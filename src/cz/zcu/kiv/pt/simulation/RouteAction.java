package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;

/**
 * Akce mrazírenského vozu. Akce jsou vytvářeny a zařazovány plánovačem.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public abstract class RouteAction {
    
    /** Čas, kdy daná akce začíná. */
    private SimulationTimestamp time;
    
    /** Výjezd. */
    private final Route route;
    
    
    /**
     * Vytvoří událost cesty mrazírenského vozu.
     * @param actRoute Výjezd vozu.
     * @param timestamp Čas události.
     */
    public RouteAction(final Route actRoute, final SimulationTimestamp timestamp) {
        this.route = actRoute;
        this.time = timestamp;
    }
    
    
    /**
     * Získá čas začátku akce.
     * @return Čas, kdy akce začíná.
     */
    public final SimulationTimestamp getTime() {
        return time;
    }
    
    /**
     * Nastaví nový čas akce.
     * @param timestamp Nový čas.
     */
    public final void setTime(final SimulationTimestamp timestamp) {
        this.time = timestamp;
    }
    
    /**
     * Získá výjezd, ke kterému je akce přiřazena.
     * @return Objekt {@code Route}.
     */
    public final Route getRoute() {
        return route;
    }
    
    
    /**
     * Vykoná akci. 
     * @param timestamp Čas, kdy k akci došlo.
     * @return {@code NULL} (= ukončení akce), nebo čas, na který se má tato akce opět zařadit (pro vícekrokové akce).
     */
    public abstract SimulationTimestamp perform(final SimulationTimestamp timestamp);
    
    /**
     * Získá vypočítanou cenu akce.
     * @return Cena akce.
     */
    public abstract double getCost();
    
    
    /**
     * Zjistí, kde se mrazírenský vůz právě nachází.
     * @return Uzel, ve kteérm se vůz nachází.
     */
    public abstract INode getCurrentNode();
}
