package cz.zcu.kiv.pt.simulation;

/**
 * Interface pro loggery událostí simulace.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public interface IEventLogger {    
    /**
     * Zapíše zprávu ze simulace do logu.
     * @param timestamp Čas události.
     * @param message Zpráva.
     */
    void writeMessage(SimulationTimestamp timestamp, String message);
    
    /**
     * Uzavře logger (= soubor, socket, stream...).
     */
    void close();
}
