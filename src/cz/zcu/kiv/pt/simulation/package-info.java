/**
 * Diskrétní simulace. Obsahuje třídy, které zajišťují generování objednávek,
 * plánování výjezdů a jejich postupné vyřizování.
 */
package cz.zcu.kiv.pt.simulation;