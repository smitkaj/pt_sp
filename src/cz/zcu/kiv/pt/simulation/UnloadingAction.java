package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;

/**
 * Akce vykládání kontejneru.
 * @author Barbora Jánská
 * @author Jan Smitka
 */
public final class UnloadingAction extends RouteAction {
    /** Uzel, ve kterém se bude nakládat. */
    private final INode node;
    
    /** Počet kontejnerů, které se mají vyložit. */
    private final byte cargo;
    
    /** Objednávka, která bude vyložením vyřízena. */
    private final Order order;
    
    /** Cena za vyložení kontejnerů. */
    private final double cost;
    
    
    /**
     * Vytvoří akci vykládání či nakládání zadaného nákladu v zadané pobočce.
     * @param route Výjezd.
     * @param time Čas události.
     * @param unloadInNode Uzel, ve kterém dojde k vykládání.
     * @param unloadCargo Počet kontejnerů, které se mají vyložit. Záporné číslo znamená nakládání.
     * @param calcCost Vypočítaná cena vykládání či nakládání.
     */
    public UnloadingAction(final Route route, final SimulationTimestamp time, final INode unloadInNode, final byte unloadCargo, final double calcCost) {
        super(route, time);
        this.node = unloadInNode;
        this.cargo = unloadCargo;
        this.order = null;
        this.cost = calcCost;
    }
    
    
    /**
     * Vytvoří akci vykládání nákladu pro danou objednávku.
     * @param route Výjezd.
     * @param time Čas události.
     * @param unloadOrder Objednávka, která bude vykládáním splněna.
     * @param calcCost Vypočítaná cena vykládání či nakládání.
     */
    public UnloadingAction(final Route route, final SimulationTimestamp time, final Order unloadOrder, final double calcCost) {
        super(route, time);
        this.node = unloadOrder.getNode();
        this.cargo = unloadOrder.getCargo();
        this.order = unloadOrder;
        this.cost = calcCost;
    }

    
    /**
     * Získá náklad, který se má vyložit či naložit. Záporné čislo znamená nakládání.
     * @return Počet kontejnerů k vyložení či naložení.
     */
    public byte getCargo() {
        return cargo;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public double getCost() {
        return cost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SimulationTimestamp perform(final SimulationTimestamp time) {
        getRoute().logMessage(Event.ACTIONS, this.toString());
        if (order != null) {
            order.setState(Order.State.Finished);
        }
        return null;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public INode getCurrentNode() {
        return node;
    }
    
    
    /**
     * Získá objednávku, která je spojena s tímto vykládáním.
     * @return {@link Objednávka}, nebo {@null}, pokud se jedná o nakládání v centrále.
     */
    public Order getOrder() {
        return order;
    }
    
    /**
     * Získá popis akce.
     * @return Popis akce.
     */
    @Override
    public String toString() {
        String event;
        int count;
        if (cargo > 0) {
            event = "Vykládání";
        } else {
            event = "Nakládání";
        }
        
        if (cargo > 0) {
            count = cargo;
        } else {
            count = -cargo;
        }
        return String.format("%s %d kontejnerů v %s", event, count, node);
    }
    
    
}
