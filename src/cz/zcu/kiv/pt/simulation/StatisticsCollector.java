package cz.zcu.kiv.pt.simulation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * Třída, která zajišťuje sběr a generování statistik.
 * Třída je plněna simulací pomocí metod {@code addOrder} a {@code addRoute}.
 * Vše ostatní si třída již zjistí sama.
 * @author Barbora Jánská
 * @author Jan Smitka
 */
public final class StatisticsCollector {
    /** Seznam objednávek. */
    private final List<Order> orders;
    
    /** Seznam výjezdů. */
    private final List<Route> routes;
    
    /** Statistiky jednotlivých zmrzlinářství. */
    private final Map<Short, NodeStatistics> nodes;
    
    /** Velikost bufferu pro kopírování šablon. */
    private static final int BUFFER_SIZE = 16 * 1024;
    
    
    /**
     * Vytvoří prázdný collector statistik.
     */
    public StatisticsCollector() {
        orders = new LinkedList<Order>();
        routes = new LinkedList<Route>();
        nodes = new TreeMap<Short, NodeStatistics>();
    }
    
    
    /**
     * Započítá do statistik novou objednávku.
     * @param order Objednávka k započítání.
     */
    public void addOrder(final Order order) {
        orders.add(order);
        short nodeId = order.getNode().getId();
        if (!nodes.containsKey(nodeId)) {
            nodes.put(nodeId, new NodeStatistics(nodeId));
        }
        nodes.get(nodeId).addOrder(order);
    }
    
    
    /**
     * Započítá do statistik nový výjezd auta.
     * @param route Výjezd k započítání.
     */
    public void addRoute(final Route route) {
        routes.add(route);
        UnloadingAction unloading;
        for (RouteAction action : route.getScheudle().getActions()) {
            if (action instanceof UnloadingAction && ((UnloadingAction) action).getCargo() > 0) {
                unloading = (UnloadingAction) action;
                nodes.get(unloading.getCurrentNode().getId()).addUnloadingAction(unloading);
            }
        }
    }

    
    /**
     * Získá statistiky jednotlivých poboček.
     * @return Mapa statistik ve tvaru {@code ID} => statistiky uzlu.
     */
    public Map<Short, NodeStatistics> getNodes() {
        return nodes;
    }

    /**
     * Získá seznam všech přijatých objednávek.
     * @return Seznam objednávek.
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * Získá seznam všech započatých výjezdů.
     * @return Seznam výjezdů.
     */
    public List<Route> getRoutes() {
        return routes;
    }
    
    
    /**
     * Vygeneruje HTML statistiky a zapíše je do zadaného výstupního souboru.
     * @param filename Výstupní soubor.
     * @throws IOException V případě, že během zápisu dojde k chybě.
     */
    public void generateStats(final String filename) throws IOException {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(filename, "UTF-8");
            
            DateFormat df = new SimpleDateFormat("EEEE d. MM. yyyy, H:mm", Locale.getDefault());
            writeTemplate("resources/stat-head.htm", writer);
            writer.format("<div class=\"page-header\"><h1>Statistiky zmrzlinářství <small>vygenerováno: %s</small></h1></div>",
                    df.format(new Date()));
            generateOrderStats(writer);
            generateRouteStats(writer);
            generateNodeStats(writer);
            writeTemplate("resources/stat-foot.htm", writer);
        } catch (UnsupportedEncodingException ex) {
            // nothing to do, since UTF-8 is required to be supported
            // see http://docs.oracle.com/javase/6/docs/api/java/nio/charset/Charset.html
            return;
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
    
    
    /**
     * Zapíše šablonu připravenou v resources do výstupního souboru.
     * @param file Resource soubor s šablonou.
     * @param dst Výstupní proud.
     * @throws IOException V případě, že během zápisu dojde k chybě.
     */
    private void writeTemplate(final String file, final Writer dst) throws IOException {
        BufferedReader template = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(file)));
        try {
            int count = 0;
            char[] buffer = new char[BUFFER_SIZE];
            while ((count = template.read(buffer)) >= 0) {
                dst.write(buffer, 0, count);
            }
        } finally {
            template.close();
        }
    }
    
    
    /**
     * Vygeneruje statistiky objednávek.
     * @param writer Výstupní proud.
     */
    private void generateOrderStats(final PrintWriter writer) {
        int rejected = 0, finished = 0, other = 0;
        int contTotal = 0, contDeliv = 0, contReje = 0;
        
        writer.println("<h2 id=\"orders\">Objednávky <small>kompletní seznam objednávek</small></h2>");
        
        for (Order order : orders) {
            contTotal += order.getCargo();
            switch (order.getState()) {
                case Rejected:
                    contReje += order.getCargo();
                    rejected++;
                    break;
                case Finished:
                    contDeliv += order.getCargo();
                    finished++;
                    break;
                default:
                    contReje += order.getCargo();
                    other++;
                    break;
            }
        }
        
        writer.format("<dl><dt>Celkem objednávek:</dt><dd>%d</dd>%n", orders.size());
        writer.format("<dt>Vyřízených objednávek:</dt><dd>%d</dd>%n", finished);
        writer.format("<dt>Odmítnutých objednávek:</dt><dd>%d</dd>%n", rejected);
        if (other > 0) {
            writer.format("<dt>Ostatní objednávky:</dt><dd>%d</dd>%n", other);
        }
        writer.format("<dt>Objednané kontejnery:</dt><dd>%d</dd>%n", contTotal);
        writer.format("<dt>Doručené kontejnery:</dt><dd>%d</dd>%n", contDeliv);
        writer.format("<dt>Nedoručené kontejnery:</dt><dd>%d</dd>%n</dl>", contReje);
        
        writer.println("<table class=\"zebra-striped condensed-table\"><thead><tr>");
        writer.println("<th>#</th><th>Čas přijetí</th><th>Pobočka</th><th>Objednáno</th><th>Stav</th>");
        writer.println("</tr></thead><tbody>");
        
        for (Order order : orders) {
            writer.format("<tr id=\"order-%d\"><td>%d</td><td>%s</td>"
                    + "<td><a href=\"#node-%d\">%s</a></td>"
                    + "<td>%d t</td>"
                    + "<td>%s</td></tr>%n",
                    order.getId(), order.getId(), order.getTime(), order.getNode().getId(),
                    order.getNode(), order.getCargo(), order.getStateLabel());
            
            
        }
        writer.println("</tbody></table>");
    }
    
    
    /**
     * Vygeneruje statistiky výhjezdů.
     * @param writer Výstupní proud.
     */
    private void generateRouteStats(final PrintWriter writer) {
        
        writer.println("<h2 id=\"routes\">Výjezdy <small>podrobný seznam výjezdů</small></h2>");
        
        NumberFormat nf = NumberFormat.getInstance();
        
        int totalCargo = 0;
        int totalDistance = 0;
        double totalCost = 0;
        writer.println("<h3>Výjezdy <small><a href=\"#routes-sum\">souhrn &raquo;</a></small></h3>");
        for (Route route : routes) {
            writer.format("<h4 id=\"route-%d\">Výjezd #%d</h4>", route.getId(), route.getId());
            
            writer.println("<table class=\"zebra-striped condensed-table\"><thead>");
            writer.println("<tr><th>Čas</th><th>Akce</th><th>Cena</th><th>&nbsp;</th></tr>");
            writer.println("</thead><tbody>");
            double cost = 0;
            byte cargo = 0;
            short distance = 0;
            for (RouteAction action : route.getScheudle().getActions()) {
                writer.format("<tr>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s Kč</td>",
                        action.getTime(), action, nf.format(action.getCost()));
                cost += action.getCost();
                if (action instanceof UnloadingAction && ((UnloadingAction) action).getCargo() > 0) {
                    cargo += ((UnloadingAction) action).getCargo();
                    Order order = ((UnloadingAction) action).getOrder();
                    writer.format("<td><a href=\"#order-%d\">Objednávka #%d</a></td>", order.getId(), order.getId());
                } else if (action instanceof TravelAction) {
                    distance += ((TravelAction) action).getDistance();
                    writer.format("<td>%s km</td>", nf.format(((TravelAction) action).getDistance()));
                } else {
                    writer.format("<td></td>");
                }
                
                writer.println("</tr>");
            }
            writer.println("</tbody></table>");
            
            writer.format("<dl><dt>Nákad:</dt><dd>%d t</dd>%n", cargo);
            writer.format("<dt>Ujetá vzdálenost:</dt><dd>%s km</dd>%n", nf.format(distance));
            writer.format("<dt>Cena:</dt><dd>%s Kč</dd>%n</dl>", nf.format(cost));
            totalCargo += cargo;
            totalDistance += distance;
            totalCost += cost;
        }
        
        writer.println("<h3 id=\"routes-sum\">Souhrn</h3>");
        
        writer.format("<dl><dt>Celkový nákad:</dt><dd>%s t</dd>%n", nf.format(totalCargo));
        writer.format("<dt>Celková ujetá vzdálenost:</dt><dd>%s km</dd>%n", nf.format(totalDistance));
        writer.format("<dt>Celková cena:</dt><dd>%s Kč</dd>%n", nf.format(totalCost));
        
        writer.format("<dt>Auto průměrně nacestovalo:</dt><dd>%s km</dd>%n", nf.format(((double) totalDistance) / routes.size()));
        writer.format("<dt>Průměrná cena na doručení 1 kontejneru:</dt><dd>%s Kč</dd>%n", nf.format(totalCost / totalCargo));
        writer.format("<dt>Průměrná vzdálenost na doručení 1 kontejneru:</dt><dd>%s km</dd>%n", nf.format(((double) totalDistance) / totalCargo));
        writer.format("<dt>Průměrný náklad auta:</dt><dd>%.3f t</dd>%n</dl>", ((double) totalCargo) / routes.size());
    }
    
    
    /**
     * Vygeneruje statistiky zmrzlinářství.
     * @param writer Výstupní proud.
     */
    private void generateNodeStats(final PrintWriter writer) {
        writer.println("<h2 id=\"nodes\">Zmrzlninářství <small>přehled obsluhy jednotlivých poboček</small></h2>");
        NumberFormat nf = NumberFormat.getInstance();
        
        writer.println("<h3>Zmrzlinářství <small><a href=\"#nodes-sum\">souhrn &raquo;</a></small></h3>");
        int totalCargo = 0;
        for (NodeStatistics node : nodes.values()) {
            writer.format("<h4 id=\"node-%d\">Zmrzlinářství #%d <small>doručeno: <strong>%s t</strong> ze <strong>%s t</strong>, </small></h4>", node.getNodeId(), node.getNodeId(),
                    nf.format(node.getCargoDelivered()), nf.format(node.getCargoTotal()));
            
            writer.println("<table class=\"zebra-striped condensed-table\"><thead>");
            writer.println("<tr><th>Čas</th><th>Výjezd</th><th>Náklad</th><th>&nbsp;</th></tr>");
            writer.println("</thead><tbody>");
            for (UnloadingAction action : node.getUnloadingActions()) {
                writer.format("<tr><td>%s</td>"
                        + "<td><a href=\"#route-%d\">%d</a></td>"
                        + "<td>%d t</td><td><a href=\"#order-%d\">Objednávka %d</a></td></tr>%n",
                        action.getTime(), action.getRoute().getId(), action.getRoute().getId(),
                        action.getOrder().getCargo(), action.getOrder().getId(), action.getOrder().getId());
            }
            writer.println("</tbody></table>");
            totalCargo += node.getCargoTotal();
        }
        
        writer.println("<h3 id=\"nodes-sum\">Souhrn</h3>");
        writer.format("<dl><dt>Zmrzlinu objednávalo:</dt><dd>%s zmrzlinářství</dd>%n", nf.format(nodes.size()));
        writer.format("<dt>Průměrná objednávka:</dt><dd>%.3f t</dd>%n</dl>", ((double) totalCargo) / orders.size());
    }
}
