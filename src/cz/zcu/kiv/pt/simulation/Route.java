package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.ListIterator;

/**
 * Výjezd mrazírenského vozu.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class Route extends Event {
    /** Čas, kdy se začlo s plánováním výjezdu. */
    private final SimulationTimestamp timeCreated;
    
    /** Číslo výjezdu. */
    private int id;
    
    /** Objekt simulace. */
    private Simulation simulation;
    
    /** Stav výjezdu. */
    private State state;
    
    /**
     * Stavy výjezdu.
     */
    public enum State {
        /** V plánování. Cesta objednávky může být upravena. */
        Planning,
        /** Na cestě. Mrazírenský vůz je na cestě mezi dvěma pobočkami. */
        Realization,
        /** Dokončeno. */
        Finished
    }
    
    
    /** Objednávky, které jsou během výjezdu obsluhovány. */
    private ArrayList<Order> orders;
    
    /** Náklad vozu. */
    private byte cargo = 0;
    
    /** Vypočítaná cena. */
    private double projectedCost;
    
    /** Časový plán. */
    private SchedulerResult scheudle;
    
    /** Poslední zpráva. */
    private String lastMessage;
    
    /** Čas výjezdu. */
    private SimulationTimestamp startTime;
    
    /** Iterátor pro frontu akcí. */
    private ListIterator<RouteAction> actionsIterator;
    
    /** Poslední vykonaná akce. */
    private RouteAction lastAction;
    
    
    /**
     * Vytvoří prázdný výjezd.
     * @param sim Objekt simulace.
     * @param time Čas, na kdy je výjezd naplánován.
     */
    public Route(final Simulation sim, final SimulationTimestamp time) {
        super(time);
        timeCreated = time;
        state = State.Planning;
        this.simulation = sim;
    }
    
    /**
     * Vytvoří výjezd pro skupinu objednávek.
     * @param sim Objekt simulace.
     * @param time Čas výjezdu.
     * @param orderList Seznam objednávek.
     */
    public Route(final Simulation sim, final SimulationTimestamp time, final Collection<? extends Order> orderList) {
        this(sim, time);
        this.orders = new ArrayList<Order>(orderList);
        for (Order order : this.orders) {
            cargo += order.getCargo();
        }
    }
    
    
    /**
     * Vytvoří výjezd pro jednu objednávku.
     * @param sim Objekt simulace.
     * @param time Čas výjezdu.
     * @param order Objednávka.
     */
    public Route(final Simulation sim, final SimulationTimestamp time, final Order order) {
        this(sim, time);
        this.orders = new ArrayList<Order>();
        this.orders.add(order);
        cargo = order.getCargo();
    }
    
    
    /**
     * Anuluje předpočítanou cenu.
     */
    public void clearStats() {
        projectedCost = 0;
    }

    
    /**
     * Získá čas, kdy se začlo s plánováním výjezdu.
     * @return Čas začátku plánování výjezdu.
     */
    public SimulationTimestamp getTimeCreated() {
        return timeCreated;
    }
    
    
    /**
     * Zjistí, zda je možné objednávku ještě přidat k této cestě.
     * @param order Objednávka.
     * @return Je možné přidat objednávku?
     */
    public boolean canHandle(final Order order) {
        return (cargo + order.getCargo()) <= simulation.getTruckMaxCargo();
    }
    
    
    /**
     * Přidá objednávku na zadaný index.
     * @param index Index.
     * @param order Objednávka.
     */
    public void addOrder(final int index, final Order order) {
        orders.add(index, order);
        cargo += order.getCargo();
        clearStats();
    }
    
    
    /**
     * Přidá objednávku na konec seznamu.
     * @param order Objednávka.
     */
    public void addOrder(final Order order) {
        addOrder(orders.size(), order);
        clearStats();
    }
    
    
    /**
     * Odebere objednávku z výjezdu.
     * @param order Objednávka.
     */
    public void removeOrder(final Order order) {
        this.cargo -= order.getCargo();
        orders.remove(order);
        clearStats();
    }
    
    
    /**
     * Vrátí ID (pořadí) výjezdu. Dostupné až po začátku výjezdu.
     * @return ID (pořadí).
     */
    public int getId() {
        return id;
    }

    
    /**
     * Získá objekt simulace.
     * @return Objekt simulace.
     */
    public Simulation getSimulation() {
        return simulation;
    }
    
    /**
     * Získá aktuální náklad mrazírenského vozu.
     * @return Náklad vozu.
     */
    public byte getCargo() {
        return cargo;
    }

    /**
     * Získá objednávky, které mrazírenský vůz na cestě vyřizuje.
     * @return Chronologický seznam objednávek.
     */
    public ArrayList<Order> getOrders() {
        return orders;
    }

    /**
     * Získá stav objednávky.
     * @return Stav.
     */
    public State getState() {
        return state;
    }

    
    /**
     * Získá plán výjezdu.
     * @return Plán výjezdu.
     */
    public SchedulerResult getScheudle() {
        return scheudle;
    }
    
    /**
     * Získá čas začátku výjezdu.
     * @return Čas začátku.
     */
    public SimulationTimestamp getStartTime() {
        return startTime;
    }

    
    /**
     * Nastaví plán výjezdu. Přepíše starý.
     * @param newScheudle Nový plán výjezdu.
     */
    public void setScheudle(final SchedulerResult newScheudle) {
        this.scheudle = newScheudle;
        setTime(newScheudle.getStartTime());
        actionsIterator = newScheudle.getActions().listIterator();
    }
    
    
    /**
     * Vypočítá odhadovanou cenu cesty. Nebere ohledy na její umístění v čase,
     * tedy nezapočítává čekání.
     * @return Odhadovaná cena.
     */
    public double getProjectedCost() {
        if (projectedCost == 0) {
            byte currentCargo = this.cargo;
            INode currentNode = simulation.getMainNode();
            for (Order order : orders) {
                projectedCost += simulation.getTravelCost(currentNode, order.getNode(), currentCargo);
                currentNode = order.getNode();
                currentCargo -= order.getCargo();
            }
            projectedCost += simulation.getTravelCost(currentNode, simulation.getMainNode(), currentCargo);
        }
        return projectedCost;
    }
    
    
    /**
     * Pokusí se objednávku zařadit do cesty a najde ideální pozici.
     * @param order Objednávka.
     * @return Nejlepší pořadí objedávky a jeho cena.
     */
    public RoutingResult findOrderPosition(final Order order) {
        int index = orders.size();
        addOrder(order);
        double minCost = getProjectedCost();
        for (int i = index - 1; i >= 0; i--) {
            clearStats();
            Collections.swap(orders, i, i + 1);
            if (getProjectedCost() < minCost) {
                minCost = getProjectedCost();
                index = i;
            }
        }
        removeOrder(order);
        
        return new RoutingResult(minCost, index);
    }
    

    /**
     * Zpracuje jeden krok cesty vozu.
     * @return Další událost v pořadí.
     */
    @Override
    public Event process() {        
        RouteAction action;
        
        if (actionsIterator.hasNext()) {
            action = actionsIterator.next();
            if (this.state == State.Planning) {
                id = simulation.routeStarted(this);
                this.state = State.Realization;
                startTime = getTime();
            }
            
            SimulationTimestamp nextTime = action.perform(getTime());
            lastAction = action;
            if (nextTime == null) {
                if (actionsIterator.hasNext()) {
                    action = actionsIterator.next();
                    setTime(action.getTime());
                    actionsIterator.previous();
                    return this;
                } else {
                    return null;
                }
                
            } else {
                actionsIterator.previous();
                setTime(nextTime);
                return this;
            }
        } else {
            return null;
        }
    }
    
    
    /**
     * Vypíše zprávu o cestě vozu do logu simulace.
     * @param priority Priorita.
     * @param message Zpráva.
     * @param args Argumenty.
     */
    public void logMessage(final int priority, final String message, final Object... args) {
        simulation.logMessage(priority, getTime(), String.format("Výjezd %d: %s", id, message), args);
        lastMessage = String.format(message, args);
    }
    
    
    /**
     * Získá poslední zprávu (= akci), která byla výjezdem vyslána.
     * @return Poslední zpráva.
     */
    public String getLastMessage() {
        return lastMessage;
    }

    /**
     * Získá poslední akci, která byla při výjezdu vykonána.
     * @return Akce.
     */
    public RouteAction getLastAction() {
        return lastAction;
    }
    
    
    /**
     * Získá popis výjezdu, v tomto případě poslední zprávu, která byla výjezdem vyslána.
     * @return Poslední zpráva.
     */
    @Override
    public String toString() {
        return lastMessage;
    }
    
    
}
