package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.IGraph;
import cz.zcu.kiv.pt.graph.INode;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.DAY;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.HOUR;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.MINUTE;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Hlavní třída simluace. Zajišťujue běh událostí na základě prioritní fronty.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class Simulation {
    /** Počáteční kapacita haldy. */
    private static final int EXPECTED_EVENTS = 1024;
    
    /** Očekávaný počet objednávek v plánování (= počáteční velikost seznamu). */
    private static final int EXPECTED_ROUTES = 256;
    
    /** Možný začátek vykládání. */
    public static final int UNLOADING_START = 6 * HOUR;
    
    /** Možný konec vykládání. */
    public static final int UNLOADING_END = 18 * HOUR;
    
    /** Doba vykládání. */
    public static final int UNLOADING_TIME = 15 * MINUTE;

    /** Čas v poslední den, kdy může přijít objednávka. */
    public static final int LAST_ORDER = 12 * HOUR;
    
    /** Střední hodnota intervalu mezi objednávkami. */
    public static final int ORDER_DELTA_MEAN = 600;    
    
    /** Minimální počet kontejnerů, které si jedno zmrzlinářství může objednat. */
    public static final byte ORDER_MIN_CARGO = 1;
    
    /** Maximální počet kontejnerů, které si jedno zmrzlinářství může objednat. */
    public static final byte ORDER_MAX_CARGO = 5;
    
    /** Rychlost mrazírenského vozu v KM/h. **/
    public static final double TRUCK_SPEED = 70;
    
    /** Maximální náklad mrazírenského vozu. */
    public static final byte TRUCK_MAX_CARGO = 10;
    
    /** Fixní cena cesty mrazírenského vozu. */
    public static final int KM_COST = 5;
    
    /** Cena přepravy 1 kontejneru o 1 km. */
    public static final int KM_COST_PER_TON = 1;
    
    /** Cena vyložení jednoho plného kontejneru. */
    public static final int UNLOADING_COST = 100;
    
    /** Cena čekání mrazírenského vozu po dobu jedné hodiny. */
    public static final int WAITING_COST = 150;
    
    /** ID centrálního uzlu. */
    public static final short CENTRAL_NODE = 0;
    
    
    
    /** Počet dnů, které budou simulovány. */
    private final byte days;
    
    /** Čas, kdy může přijít poslední objednávka. */
    private final int lastOrderTime;
    
    /** Čas, do kdy je nutné doručit poslední objednávku. */
    private final int lastDeliveryTime;
    
    /** Fronta událostí. */
    private final PriorityQueue<Event> events;
    
    /** Generátor náhodných čísel. */
    private final Generator generator;
    
    /** Loggery událostí. */
    private final ArrayList<IEventLogger> loggers;
    
    /** Graf reprezentující zmrzlinářství. */
    private final IGraph graph;
    
    /** Centrála. */
    private final INode mainNode;
    
    /** Ukecanost simulace. */
    private int verbosity = Event.INNER_ACTIONS;
    
    
    /** Výjezdy v plánování. */
    private final ArrayList<Route> plannedRoutes;

    /** Plánovač. */
    private final Scheduler scheduler;
    
    /** Sběrač a generátor statistik. */
    private final StatisticsCollector statistics;
    
    /** Aktuální čas. */
    private SimulationTimestamp currentTime;
    
    /** Počet výjezdů. */
    private int routes = 0;
    
    /** Počet objednávek. */
    private int orders = 0;
    
    
    /**
     * Inicializuje simulaci.
     * @param worldGraph Graf reprezentující svět.
     * @param daysToSimulate Počet dnů, které budou simulovány.
     */
    public Simulation(final IGraph worldGraph, final byte daysToSimulate) {
        events = new PriorityQueue<Event>(EXPECTED_EVENTS);
        generator = new Generator();
        loggers = new ArrayList<IEventLogger>();
        
        this.graph = worldGraph;
        mainNode = worldGraph.getNode(CENTRAL_NODE);
        
        this.days = daysToSimulate;
        lastOrderTime = (daysToSimulate - 1) * DAY + LAST_ORDER;
        lastDeliveryTime = (daysToSimulate - 1) * DAY + UNLOADING_END;
        
        plannedRoutes = new ArrayList<Route>(EXPECTED_ROUTES);
        
        scheduler = new Scheduler(this);
        statistics = new StatisticsCollector();
        
        currentTime = new SimulationTimestamp(0);
    }
    
    
    
    /**
     * Získá odkaz na centrálu.
     * @return Centrála.
     */
    public INode getMainNode() {
        return mainNode;
    }

    
    /**
     * Získá frontu událostí.
     * @return Fronta událostí.
     */
    public PriorityQueue<Event> getEvents() {
        return events;
    }
    
    
    /**
     * Zařadí objednávku k vyřízení do fronty událostí.
     * @param newOrder Nová objednávka.
     */
    public void addOrder(final Order newOrder) {
        events.add(newOrder);
        statistics.addOrder(newOrder);
    }
    
    
    /**
     * Získá sběrač a generátor statistik.
     * @return Generátor statistik.
     */
    public StatisticsCollector getStatistics() {
        return statistics;
    }

    
    /**
     * Získá aktuální čas simulace (= čas poslední zpracované události).
     * @return Aktuální čas.
     */
    public SimulationTimestamp getCurrentTime() {
        return currentTime;
    }
    
    /**
     * Získá z grafu pobočku s daným ID.
     * @param id ID pobočky.
     * @return Uzel s daným ID.
     */
    public INode getNode(final short id) {
        return graph.getNode(id);
    }
    
    
    /**
     * Získá počet uzlů v grafu.
     * @return Počet uzlů.
     */
    public short getGraphSize() {
        return graph.getSize();
    }
    
    
    /**
     * Získá mezní čas vyložení nákladu.
     * @return Čas v sekundách.
     */
    public int getLastDeliveryTime() {
        return lastDeliveryTime;
    }
    
    
    /**
     * Získá délku intervalu, ve kterém je možno vykládat náklad.
     * @return Délka intervalu v sekundách.
     */
    public int getUnloadTimespan() {
        return UNLOADING_END - UNLOADING_START;
    }
    
    /**
     * Získá délku intervalu, ve kterém <b>není</b> možno vykládat náklad.
     * @return Délka intervalu v sekundách.
     */
    public int getNonUnloadTimespan() {
        return DAY - getUnloadTimespan();
    }
    
    
    /**
     * Získá čas, kdy má simulace skončit.
     * @return Čas konce simulace v sekundách.
     */
    public int getSimulationEnd() {
        return days * DAY;
    }

    
    /**
     * Získá podrobnost výpisů simulace.
     * @return Konstanta {@code Event.SIMULATION}, {@code Event.EVENTS}, {@code Event.ACTIONS}, nebo {@code Event.INNER_ACTIONS}.
     */
    public int getVerbosity() {
        return verbosity;
    }

    /**
     * Nastaví podrobnost výpisů simulace. Jednotlivé úrovně jsou udány konstantami
     * {@code Event.SIMULATION}, {@code Event.EVENTS}, {@code Event.ACTIONS}, {@code Event.INNER_ACTIONS}.
     * @param newVerbosity Podrobnost výpisů.
     */
    public void setVerbosity(final int newVerbosity) {
        this.verbosity = newVerbosity;
    }
    
    
    
    /**
     * Zaregistruje k simulaci nový logger.
     * @param logger Logger.
     */
    public void attachLogger(final IEventLogger logger) {
        loggers.add(logger);
    }
    
    
    /**
     * Uzavře všechny zaregistrované loggery.
     */
    public void closeLoggers() {
        for (IEventLogger logger : loggers) {
            logger.close();
        }
    }
    
    /**
     * Zapíše zprávu do všech zaregistrovaných loggerů.
     * 
     * @param priority Priorita zprávy.
     * @param time Čas události.
     * @param message Zpráva.
     * @param args Parametry.
     */
    public void logMessage(final int priority, final SimulationTimestamp time, final String message, final Object... args) {
        if (priority <= verbosity) {
            for (IEventLogger logger : loggers) {
                logger.writeMessage(time, String.format(message, args));
            }
        }
    }
    
    
    /**
     * Uzavře všechny zaregistrované loggery.
     */
    public void closeLogger() {
        for (IEventLogger logger : loggers) {
            logger.close();
        }
    }
    
    
    /**
     * Vygeneruje zadaný počet objednávek v čase 0.
     * @param num Počet objednávek.
     */
    public void generateOrders(final int num) {
        generateOrders(num, new SimulationTimestamp(0));
    }
    
    
    /**
     * Vygeneruje zadaný počet objednávek na zadaný čas.
     * @param num Počet objednávek.
     * @param time Čas, ve kterém budou objednávky vygenerovány.
     */
    public void generateOrders(final int num, final SimulationTimestamp time) {
        for (int i = 0; i < num; i++) {
            events.add(generateOrder(time));
        }
        logMessage(Event.SIMULATION, time, "Vygenerováno %d objednávek.", num);
    }
    
    
    /**
     * Přidá breakpoint do simulace.
     * @param breakpoint Breakpoint s časem zastavení.
     */
    public void addBreakpoint(final Breakpoint breakpoint) {
        events.offer(breakpoint);
    }
    
    /**
     * Spustí simulaci.
     * @throws Exception Pokud dojde během simulace k výjimce.
     */
    public void run() throws Exception {
        events.add(nextOrder(currentTime));
        
        Event currentEvent, nextEvent;
        try {
            //SimulationTimestamp prevTime = new SimulationTimestamp(0);
            while (!events.isEmpty()) {
                currentEvent = events.poll();
                currentTime = currentEvent.getTime();
                /*if (prevTime.compareTo(currentEvent.getTime()) > 0) {
                    throw new Exception();
                }
                prevTime = currentEvent.getTime();*/
                nextEvent = currentEvent.process();
                if (nextEvent != null) {
                    events.offer(nextEvent);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            closeLoggers();
        }
    }
    
    
    /**
     * Zjistí vzdálenost mezi dvěma uzly.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     * @return Vzdálenost mezi uzly.
     */
    public int getDistance(final INode from, final INode to) {
        return graph.getDistance(from, to);
    }
    
    
    
    /**
     * Zjistí, kolik uveze mrazírenský vůz.
     * @return Nosnost mrazírenského vozu.
     */
    public byte getTruckMaxCargo() {
        return TRUCK_MAX_CARGO;
    }
    
    
    /**
     * Vypočítá čas, který mrazírenský vůz potřebuje na překonání určité vzdálenosti.
     * @param distance Vzdálenost.
     * @return Čas v sekundách.
     */
    public int getTravelTime(final int distance) {
        return (int) Math.round((distance / TRUCK_SPEED) * HOUR);
    }
    
    /**
     * Vypočítá čas, který mrazírenský vůz potřebuje na překonání vzdálenosti mezi dvěma uzly.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     * @return Čas v sekundách.
     */
    public int getTravelTime(final INode from, final INode to) {
        return getTravelTime(graph.getDistance(from, to));
    }
    
    
    /**
     * Získá z grafu cestu z uzlu do uzlu.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     * @return Fronta s cestou.
     */
    public Queue<INode> getPath(final INode from, final INode to) {
        return graph.getPath(from, to);
    }
    
    
    /**
     * Vypočítá čas potřebný k vyložení či naložení určitého množství kontejnerů.
     * @param cargo Počet kontejnerů.
     * @return Čas vykládání či nakládání.
     */
    public int getUnloadingTime(final byte cargo) {
        byte absCargo = cargo;
        if (cargo < 0) {
            absCargo = (byte) -cargo;
        }
        return UNLOADING_TIME * absCargo;
    }
    
    
    /**
     * Vypočítá cenu za vyložení či naložení určitého množství kontejnerů.
     * @param cargo Počet kontejnerů.
     * @return Cena za nakládání či vykládání.
     */
    public int getUnloadingCost(final byte cargo) {
        byte absCargo = cargo;
        if (cargo < 0) {
            absCargo = (byte) -cargo;
        }
        return UNLOADING_COST * absCargo;
    }
    
    
    /**
     * Vypočítá náklady mrazírenského vozu na překonání určité vzdálenosti.
     * @param distance Vzdálenost.
     * @param cargo Náklad (počet kontejnerů).
     * @return Celkové náklady na cestu.
     */
    public int getTravelCost(final int distance, final byte cargo) {
        return distance * KM_COST + cargo * distance * KM_COST_PER_TON;
    }
    
    /**
     * Vypočítá náklady na cestu mezi 2 uzly.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     * @param cargo Náklad (počet kontejnerů).
     * @return Celkové náklady na cestu.
     */
    public int getTravelCost(final INode from, final INode to, final byte cargo) {
        return getTravelCost(getDistance(from, to), cargo);
    }
    
    
    /**
     * Vypočítá cenu čekání o zadaný čas.
     * @param duration Doba čekání.
     * @return Cena čekání.
     */
    public double getWaitCost(final int duration) {
        return (duration / (double) HOUR) * WAITING_COST;
    }
    
    
    
    /**
     * Ověří, zda je možné v daném čase vyložit náklad.
     * @param time Čas.
     * @return Je možné vyložit náklad?
     */
    public boolean timeIsUnloadable(final SimulationTimestamp time) {
        int seconds = time.getSeconds();
        return (seconds >= UNLOADING_START) && (seconds <= UNLOADING_END);
    }
    
    /**
     * Ověří, zda je možné objednávku doručit. Ověřuje případ, kdy se rovnou vyšle
     * do pobočky mrazírenský vůz.
     * @param order Objednávka.
     * @return Je možné objednávku splnit?
     */
    public boolean orderIsDeliverable(final Order order) {
        int now = order.getTimeRecieved().getTime();
        int duration = getTravelTime(mainNode, order.getNode());
        int unloadingTime = getUnloadingTime(order.getCargo());
        
        return ((now + duration + unloadingTime) < lastDeliveryTime) && ((now + 2 * duration + 2 * unloadingTime) < getSimulationEnd());
    }
    
    
    /**
     * Vygeneruje další objednávku v pořadí. Respektuje při tom intervaly mezi objednávkami
     * a ověřuje čas poslední objednávky. Pokud již žádná objednávka přijít nemá,
     * nebude vygenerována.
     * @param now Aktuální čas simulace.
     * @return Nová objednávka nebo {@code null}.
     */
    public Order nextOrder(final SimulationTimestamp now) {
        int delta = generator.exponentialDistribution(ORDER_DELTA_MEAN);
        SimulationTimestamp newTime = new SimulationTimestamp(now.getTime() + delta);
        if (newTime.getTime() > lastOrderTime) {
            return null;
        }
        
        Order order = generateOrder(newTime);
        order.setGenerated(true);
        return order;
    }
    
    
    /**
     * Vygeneruje novou objednávku.
     * @param time Čas, ve kterém bude objednávka vytvořena.
     * @return Nová objednávka, nebo {@code null}, pokud už žádná nemá přijít.
     */
    public Order generateOrder(final SimulationTimestamp time) {        
        byte cargo = (byte) generator.uniformDistribution(ORDER_MIN_CARGO, ORDER_MAX_CARGO);
        INode node;
        do {
            // Zmrzlinářství s ID 0 je centrála, proto vybíráme od 1
            node = graph.getNode((short) generator.uniformDistribution(1, graph.getSize() - 1));
        } while (node.getLastOrderTime() != null && node.getLastOrderTime().getDay() == time.getDay());
        node.setLastOrderTime(time);
        
        Order newOrder = new Order(this, time, node, cargo);
        statistics.addOrder(newOrder);
        return newOrder;
    }
    
    /**
     * Zajistí odebrání mrazírenského vozu ze seznamu naplánovaných a vrátí
     * pořadí výjezdu.
     * @param route Výjezd vozu.
     * @return Pořadí výjezdu.
     */
    public int routeStarted(final Route route) {
        this.plannedRoutes.remove(route);
        statistics.addRoute(route);
        return ++routes;
    }
    
    
    /**
     * Zařadí objednávku do plánu.
     * @param order Objednávka k naplánování.
     * @return Trasa, do které byla objednávka zařazena.
     */
    public Route enqueueOrder(final Order order) {
        Route newRoute = new Route(this, order.getTimeRecieved(), order);
        double newRouteCost = newRoute.getProjectedCost();
        double originalCost;
        Route bestRoute = null;
        RoutingResult bestPlannedCost = new RoutingResult();
        RoutingResult currentCost;
        
        for (Route route : plannedRoutes) {
            if (route.canHandle(order)) {
                originalCost = route.getProjectedCost();
                currentCost = route.findOrderPosition(order);
                route.addOrder(currentCost.getIndex(), order);
                if (currentCost.getCost() < (newRouteCost + originalCost) && currentCost.getCost() < bestPlannedCost.getCost()) {
                    SchedulerResult result = scheduler.schedule(route, order.getTime());
                    if (result.getCost() < bestPlannedCost.getCost()) {
                        currentCost.setResult(result);
                        bestPlannedCost = currentCost;
                        bestRoute = route;
                    }
                }
                route.removeOrder(order);
            }
        }
        
        order.setState(Order.State.Planned);
        if (bestRoute == null) {
            logMessage(Event.EVENTS, order.getTime(), "Pro objednávku vytvořen nový výjezd s cenou %f", newRouteCost);
            
            SchedulerResult res = scheduler.schedule(newRoute, order.getTime());
            newRoute.setScheudle(res);
            
            events.offer(newRoute);
            plannedRoutes.add(newRoute);
            return newRoute;
        } else {
            logMessage(Event.EVENTS, order.getTime(), "Objednávka zařazena do již existující trasy s cenou %f", bestPlannedCost.getCost());

            bestRoute.addOrder(bestPlannedCost.getIndex(), order);
            bestRoute.setScheudle(bestPlannedCost.getResult());
            
            events.remove(bestRoute);
            events.offer(bestRoute);
            
            return bestRoute;
        }
    }

    
    /**
     * Získá ID pro právě příchozí objednávku.
     * @return Nové ID.
     */
    public int acquireOrderId() {
        return ++orders;
    }
    
}
