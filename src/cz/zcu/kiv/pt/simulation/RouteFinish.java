package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;

/**
 * Konec výjezdu mrazírenského vozu.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class RouteFinish extends RouteAction {
    /** Koncový uzel. Vždy centrála. */
    private final INode node;

    /**
     * Vytvoří novou akci reprezentující konec výjezdu mrazírenského vozu.
     * @param route Výjezd.
     * @param time Čas ukončení.
     * @param finishNode Koncový uzel.
     */
    public RouteFinish(final Route route, final SimulationTimestamp time, final INode finishNode) {
        super(route, time);
        this.node = finishNode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public INode getCurrentNode() {
        return node;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getCost() {
        return 0.0;
    }
    
    

    /**
     * {@inheritDoc}
     */
    @Override
    public SimulationTimestamp perform(final SimulationTimestamp time) {
        getRoute().logMessage(Event.EVENTS, "Výjezd ukončen.");
        return null;
    }

    /**
     * Získá popis akce.
     * @return Popis akce.
     */
    @Override
    public String toString() {
        return "Konec výjezdu.";
    }
    
    
    
}
