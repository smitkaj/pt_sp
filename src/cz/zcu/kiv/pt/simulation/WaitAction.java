package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;

/**
 * Akce čekání mrazírenského vozu.
 * @author Barbora Jánská
 * @author Jan Smitka
 */
public final class WaitAction extends RouteAction {
    /** Čas, po který se má čekat. */
    private final int duration;
    
    /** Uzel, ve kterém se čeká. */
    private final INode node;
    
    /** Cena za čekání. */
    private final double cost;
    
    
    /**
     * Vytvoří akci čekání mrazírenského vozu.
     * @param route Výjezd.
     * @param time Čas začátku čekání.
     * @param waitInNode Uzel, ve kterém bude probíhat čekání.
     * @param waitDuration Délka čekání.
     * @param calcCost Vypočítaná cena čekání.
     */
    public WaitAction(final Route route, final SimulationTimestamp time, final INode waitInNode, final int waitDuration, final double calcCost) {
        super(route, time);
        this.duration = waitDuration;
        this.node = waitInNode;
        this.cost = calcCost;
    }
    
    
    /**
     * Zjistí dobu čekání.
     * @return Doba čekání.
     */
    public int getDuration() {
        return duration;
    }
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public double getCost() {
        return cost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SimulationTimestamp perform(final SimulationTimestamp time) {
        getRoute().logMessage(Event.ACTIONS, "Čekání %d sekund.", duration);
        
        return null;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public INode getCurrentNode() {
        return node;
    }

    /**
     * Získá popis akce.
     * @return Popis akce.
     */
    @Override
    public String toString() {
        return String.format("Čekání %d sekund.", duration);
    }
}
