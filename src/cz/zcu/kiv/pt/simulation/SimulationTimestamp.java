package cz.zcu.kiv.pt.simulation;

/**
 * Třída reprezentující čas v simulaci.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class SimulationTimestamp implements Comparable<SimulationTimestamp> {
    
    /** Den. */
    public static final int DAY = 86400;
    
    /** Hodina. */
    public static final int HOUR = 3600;
    
    /** Minuta. */
    public static final int MINUTE = 60;
    
    /** Magické číslo pro hashovací funkci - počáteční hodnota. */
    private static final int HASH_INIT = 7;
    
    /** Magické číslo pro hashovací funkci - multiplikátor. */
    private static final int HASH_MAGIC = 53;
    
    /** Počet sekund od začátku simulace. */
    private final int time;
    
    /**
     * Vytvoří novou obálku na čas.
     * @param simulationTime Počet sekund od začátku simulace.
     */
    public SimulationTimestamp(final int simulationTime) {
        this.time = simulationTime;
    }
    
    /**
     * Vytvoří novou obálku na čas.
     * @param day Den simulace.
     * @param seconds Počet sekund, které uběhly v daném dnu.
     */
    public SimulationTimestamp(final int day, final int seconds) {
        this((day - 1) * DAY + seconds);
    }
    
    
    /**
     * Získá čas od začátku simulace v sekundách.
     * @return Čas v sekundách.
     */
    public int getTime() {
        return time;
    }
    
    /**
     * Získá den.
     * @return Den simulace. 
     */
    public short getDay() {
        return (short) (this.time / DAY + 1);
    }
    
    
    /**
     * Získá počet sekund, které uběhly v daném dnu.
     * @return Počet sekund v daném dnu.
     */
    public int getSeconds() {
        return this.time % DAY;
    }
    
    
    /**
     * K času připočte určitý počet sekund a vrátí nový čas.
     * @param seconds Počet sekund.
     * @return Nový čas.
     */
    public SimulationTimestamp addTime(final int seconds) {
        return new SimulationTimestamp(time + seconds);
    }
    
    
    /**
     * Získá rozdíl mezi dvěma časy.
     * @param prev Předchozí čas.
     * @return Rozdíl v sekundách.
     */
    public int diff(final SimulationTimestamp prev) {
        return time - prev.getTime();
    }

    /**
     * Porovná dva časy.
     * @param obj Druhý objekt.
     * @return Rovnají se časy?
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimulationTimestamp other = (SimulationTimestamp) obj;
        if (this.time != other.time) {
            return false;
        }
        return true;
    }

    /**
     * Vypočítá hash (automaticky generováno).
     * @return Hash code.
     */
    @Override
    public int hashCode() {
        int hash = HASH_INIT;
        hash = HASH_MAGIC * hash + this.time;
        return hash;
    }

    
    /**
     * Porovná čas s jiným objektem {@code SimulationTime}.
     * @param object Druhý objekt.
     * @return Kladné číslo, nula, záporné číslo, pokud je tento čas
     * větší než, rovný, menší než druhý čas.
     */
    @Override
    public int compareTo(final SimulationTimestamp object) {
        return time - object.time;
    }
    
    
    /**
     * Převede čas na jednoduše čitelný formát.
     * @return Human-readable string.
     */
    @Override
    public String toString() {
        int seconds = this.getSeconds();
        
        return String.format("%d. den, %2d:%02d:%02d", this.getDay(), seconds / HOUR, (seconds % HOUR) / MINUTE, seconds % MINUTE);
    }
    
    
    /**
     * Najde {@code SimulationTimestamp} s minimální hodnotou.
     * @param time Pole objektů {@code SimulationTimestamp}.
     * @return Nejmenší čas.
     */
    public static SimulationTimestamp min(final SimulationTimestamp... time) {
        SimulationTimestamp min = time[0];
        for (int i = 1; i < time.length; i++) {
            if (time[i].compareTo(min) < 0) {
                min = time[i];
            }
        }
        return min;
    }
    
    /**
     * Najde {@code SimulationTimestamp} s maximální hodnotou.
     * @param time Pole objektů {@code SimulationTimestamp}.
     * @return Největší čas.
     */
    public static SimulationTimestamp max(final SimulationTimestamp... time) {
        SimulationTimestamp max = time[0];
        for (int i = 1; i < time.length; i++) {
            if (time[i].compareTo(max) > 0) {
                max = time[i];
            }
        }
        return max;
    }
}
