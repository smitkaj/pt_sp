package cz.zcu.kiv.pt.simulation.loggers;

import cz.zcu.kiv.pt.simulation.IEventLogger;
import cz.zcu.kiv.pt.simulation.SimulationTimestamp;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Logger pro výpis událostí do tabulky.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class TableLogger implements IEventLogger {   
    /** Připojený model tabulky. */
    private final DefaultTableModel model;
    
    
    /**
     * Vytvoří nový logger připojený na zadanou tabulku.
     * @param eventTable Tabulka událostí.
     */
    public TableLogger(final JTable eventTable) {
        model = (DefaultTableModel) eventTable.getModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        // není co zavřít
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeMessage(final SimulationTimestamp timestamp, final String message) {
        model.addRow(new Object[]{timestamp, message});
    }
    
}
