package cz.zcu.kiv.pt.simulation.loggers;

import cz.zcu.kiv.pt.simulation.IEventLogger;
import cz.zcu.kiv.pt.simulation.SimulationTimestamp;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Souborový logger.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class FileLogger implements IEventLogger {
    /** Writer pro zápis do souboru. */
    private final PrintWriter writer;
    
    /**
     * Připraví logger pro zápis do souboru.
     * @param filename Název souboru.
     * @throws IOException Chyba při zápisu do souboru.
     */
    public FileLogger(final String filename) throws IOException {
        writer = new PrintWriter(new BufferedWriter(new FileWriter(filename)));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void writeMessage(final SimulationTimestamp timestamp, final String message) {
        writer.format("[%s] %s\n", timestamp, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        writer.close();
    }
    
}
