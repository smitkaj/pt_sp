package cz.zcu.kiv.pt.simulation.loggers;

import cz.zcu.kiv.pt.simulation.IEventLogger;
import cz.zcu.kiv.pt.simulation.SimulationTimestamp;

/**
 * Zajišťuje zápis událostí na stdout.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class StdOutLogger implements IEventLogger {

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeMessage(final SimulationTimestamp time, final String message) {
        System.out.format("[%s] %s\n", time, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        // stdout není nutno zavírat
    }
    
}
