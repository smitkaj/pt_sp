/**
 * Třídy zajišťující výstup záznamu simulace. Jsou připraveny třídy pro výstup
 * do souboru, do konzole a do tabulky {@code JTable}.
 */
package cz.zcu.kiv.pt.simulation.loggers;