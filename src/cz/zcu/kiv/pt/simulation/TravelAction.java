package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;
import java.util.Queue;

/**
 * Akce reprezentující cestu mrazírenského vozu.
 * @author Barbora Jánská
 * @author Jan Smitka
 */
public final class TravelAction extends RouteAction {
    /** Pomocný objekt simulace. */
    private final Simulation simulation;
    
    /** Počáteční uzel. */
    private final INode startNode;

    /** Koncový uzel. */
    private final INode endNode;
    
    /** Vzdálenost mezi uzly. */
    private final int distance;
    
    /** Doba potřebná pro překonání vzdálenosti. */
    private final int duration;
    
    /** Cena za cestu. */
    private final double cost;
    
    /** Aktuální uzel. */
    private INode currentNode;
    
    /** Cesta mezi uzly. Lazy-initialized. */
    private Queue<INode> path;
    
    
    /**
     * Vytvoří novou cestu mrazírenského vozu mezi dvěma uzly.
     * @param actRoute Výjezd mrazírenského vozu.
     * @param simTime Čas začátku cesty.
     * @param travStartNode Počáteční uzel.
     * @param travEndNode Koncový uzel.
     * @param calcDistance Předpočítaná vzdálenost.
     * @param calcDuration Předpočítaná doba cesty.
     * @param calcCost Předpočítaná cena cesty.
     */
    public TravelAction(final Route actRoute, final SimulationTimestamp simTime, final INode travStartNode, final INode travEndNode, final int calcDistance, final int calcDuration, final double calcCost) {
        super(actRoute, simTime);
        this.startNode = travStartNode;
        this.endNode = travEndNode;
        this.distance = calcDistance;
        this.duration = calcDuration;
        this.cost = calcCost;
        this.simulation = actRoute.getSimulation();
    }
    

    /**
     * Zjistí vzdálenost mezi uzly.
     * @return Vzdálenost.
     */
    public int getDistance() {
        return distance;
    }

    /**
     * Zjistí dobu cesty mezi uzly.
     * @return Doba cesty.
     */
    public int getDuration() {
        return duration;
    }
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public double getCost() {
        return cost;
    }

    
    /**
     * Zjistí koncový uzel.
     * @return Koncový uzel.
     */
    public INode getEndNode() {
        return endNode;
    }

    
    /**
     * Zjistí počáteční uzel.
     * @return Počáteční uzel.
     */
    public INode getStartNode() {
        return startNode;
    }

    /**
     * Získá aktuálně načtenou cestu.
     * @return Aktuální fronta cesty.
     */
    public Queue<INode> getPath() {
        return path;
    }
    
    /**
     * Získá původní předpočítanou cestu.
     * @return Původní fronta cesty.
     */
    public Queue<INode> getOriginalPath() {
        return simulation.getPath(startNode, endNode);
    }
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public SimulationTimestamp perform(final SimulationTimestamp time) {
        if (path == null) {
            path = getOriginalPath();
            getRoute().logMessage(Event.ACTIONS, "Výjezd z %s do %s.", startNode, endNode);
        }
        
        currentNode = path.poll();
        if (path.peek() == null) {
            return null;
        } else {
            getRoute().logMessage(Event.INNER_ACTIONS, "Přesun: z %s do %s (%s > %s).", currentNode, path.peek(), startNode, endNode);
            if (path.size() > 1) {
                return time.addTime(getRoute().getSimulation().getTravelTime(currentNode, path.peek()));
            } else {
                return null;
            }
        }
    }

    
    /**
     * {@inheritDoc} 
     */
    @Override
    public INode getCurrentNode() {
        return currentNode;
    }

    /**
     * Vrátí popisku akce.
     * @return Popis akce.
     */
    @Override
    public String toString() {
        return String.format("Cesta z %s do %s.", startNode, endNode);
    }
}
