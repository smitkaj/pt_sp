package cz.zcu.kiv.pt.simulation;

import cz.zcu.kiv.pt.graph.INode;

/**
 * Objednávka.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class Order extends Event {
    /** ID objednávky. */
    private int id;
    
    /** Objekt simulace. */
    private Simulation simulation;
    
    /** Stav objednávky. */
    private State state;
    
    /**
     * Stavy objednávky.
     */
    public enum State {
        /** Vygenerována, čeká, než bude odeslána simulaci. */
        Waiting,
        /** Přijata. */
        Accepted,
        /** Odmítnuta, nelze splnit. */
        Rejected,
        /** Naplánováno (= zařazeno do cesty vozu). */
        Planned,
        /** V realizaci, vůz vyjel. */
        Realization,
        /** Vyřízená. */
        Finished
    }
    
    /** Čas, kdy objednávka dorazila (vygenerována). */
    private SimulationTimestamp timeRecieved;
    
    /** Zmrzlinářství, které náklad objednává. */
    private INode node;
    
    /** Počet objednaných kotejnerů. */
    private byte cargo;
    
    /** Indikuje, zda byla tato objednávka vygenerována generátorem dalších objednávek, nebo je výsledkem vstupu uživatele. */
    private boolean generated = false;
    
    
    /**
     * Vytvoří objednávku, která čeká na přijetí či odmítnutí.
     * @param sim Objekt simulace.
     * @param time Čas, kdy objednávka dorazila.
     * @param orderNode Pobočka, která objednává.
     * @param orderCargo Objednaný náklad.
     */
    public Order(final Simulation sim, final SimulationTimestamp time, final INode orderNode, final byte orderCargo) {
        super(time);
        this.timeRecieved = time;
        this.state = State.Waiting;
        this.simulation = sim;
        
        this.node = orderNode;
        this.cargo = orderCargo;
    }
    
    
    /**
     * Vytvoří objednávku, která čeká na přijetí či odmítnutí. Čas je zadán
     * v sekundách jako {@code int}.
     * @param sim Objekt simulace.
     * @param time Počet sekund reprezentující čas, kdy objednávka dorazila.
     * @param orderNode Pobočka, která objednává.
     * @param orderCargo Objednaný náklad.
     */
    public Order(final Simulation sim, final int time, final INode orderNode, final byte orderCargo) {
        this(sim, new SimulationTimestamp(time), orderNode, orderCargo);
    }

    
    /**
     * Zpracuje objednávku.
     * @return Nová objednávka, nebo @{code null}.
     */
    @Override
    public Event process() {
        id = simulation.acquireOrderId(); 
        if (simulation.orderIsDeliverable(this)) {
            state = State.Accepted;
            simulation.logMessage(Event.EVENTS, getTime(), "Objednávka přijata: %d objednává %d t.", node.getId(), cargo);
            simulation.enqueueOrder(this);
        } else {
            state = State.Rejected;
            simulation.logMessage(Event.EVENTS, getTime(), "Objednávka odmítnuta: %d objednává %d t.", node.getId(), cargo);
        }
        
        if (generated) {
            return simulation.nextOrder(getTime());
        } else {
            return null;
        }
    }

    
    
    /**
     * Získá počet objednaných kontejnerů.
     * @return Počet kontejnerů.
     */
    public byte getCargo() {
        return cargo;
    }
    
    
    /**
     * Získá pobočku, která si náklad objednala.
     * @return Uzel grafu (= pobočka).
     */
    public INode getNode() {
        return node;
    }

    /**
     * Získá stav objednávky.
     * @return Stav objednávky.
     */
    public State getState() {
        return state;
    }
    
    /**
     * Nastaví nový stav objednávky.
     * @param newState Nový stav.
     */
    public void setState(final State newState) {
        state = newState;
    }
    
    
    /**
     * Získá textový popis stavu objednávky.
     * @return Popis stavu.
     */
    public String getStateLabel() {
        switch(state) {
            case Waiting:
                return "Čeká na odeslání";
            case Accepted:
                return "Přijato";
            case Rejected:
                return "Odmítnuto";
            case Planned:
                return "Naplánováno";
            case Realization:
                return "V realizaci";
            case Finished:
                return "Vyřízeno";
            default:
                return "Ztraceno v časoprostoru";
                // because this shall never happen...
        }
    }
    
    
    
    /**
     * Získá ID objednávky (= pořadové číslo).
     * @return ID objednávky.
     */
    public int getId() {
        return id;
    }

    /**
     * Získá čas, kdy byla objednávka přijata.
     * @return Čas přijetí.
     */
    public SimulationTimestamp getTimeRecieved() {
        return timeRecieved;
    }

    
    /**
     * Zjistí, zda byla objednávka vygenerována generátorem dalších objednávek (s exp rozdělením),
     * nebo byla vytvořena na základě vstupu od uživatele.
     * @return Byla vytvořena generátorem?
     */
    public boolean isGenerated() {
        return generated;
    }

    
    /**
     * Nastaví příznak objednávky, která byla vygenerována generátorem dalších objednávek.
     * @param isGenerated Byla vytvořena generátorem?
     */
    public void setGenerated(final boolean isGenerated) {
        this.generated = isGenerated;
    }   
    
}
