package cz.zcu.kiv.pt.simulation;

/**
 * Rozhraní reprezentující událost v simulaci.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public abstract class Event implements Comparable<Event> {
    /** Zpráva simulace. */
    public static final int SIMULATION = 1;
    /** Zpráva událostí. */
    public static final int EVENTS = 2;
    /** Zpráva akce mrazírenského vozu. */
    public static final int ACTIONS = 3;
    /** Zpráva uvnitř akce mrazínrenského vozu. */
    public static final int INNER_ACTIONS = 4;
    
    
    /** Čas události. */
    private SimulationTimestamp time;
    
    /**
     * Výchozí konstruktor, inicializuje čas události.
     * @param timestamp Čas, na který je událost naplánovaná.
     */
    public Event(final SimulationTimestamp timestamp) {
        this.time = timestamp;
    }
    
    /**
     * Vytvoří událost, čas je zadán v sekundách.
     * @param seconds Čas v sekundách.
     */
    public Event(final int seconds) {
        this(new SimulationTimestamp(seconds));
    }
    
    
    /**
     * Zpracuje událost.
     * @return Událost, která bude zařazena do fronty událostí, nebo @{code null}.
     * 
     */
    public abstract Event process();
    
    
    /**
     * Získá čas, na který je událost naplánovaná.
     * @return Čas události.
     */
    public final SimulationTimestamp getTime() {
        return time;
    }
    
    /**
     * Nastaví nový čas události.
     * @param newTime Nový čas.
     */
    public final void setTime(final SimulationTimestamp newTime) {
        time = newTime;
    }
    
    /**
     * Srovnání časů dvou událostí.
     * @param object Druhá událost.
     * @return Kladné číslo, nula, záporné číslo, pokud je tato událost naplánovaná
     * později, stejně, nebo před druhou událostí.
     */
    @Override
    public final int compareTo(final Event object) {
        return time.compareTo(object.time);
    }
}
