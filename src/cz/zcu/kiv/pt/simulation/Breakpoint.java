package cz.zcu.kiv.pt.simulation;

/**
 * Událost na pozastavení simulace.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public abstract class Breakpoint extends Event {
    /** Objekt simulace. */
    private Simulation simulation;
    
    /**
     * Vytvoří breakpoint v zadaném čase.
     * @param seconds Čas zastavení.
     */
    public Breakpoint(final int seconds) {
        super(seconds);
    }

    /**
     * Vytvoří breakpoint pro zadanou simulaci v zadaném čase.
     * @param sim Objekt simulace.
     * @param timestamp Čas zastavení.
     */
    public Breakpoint(final Simulation sim, final SimulationTimestamp timestamp) {
        super(timestamp);
        simulation = sim;
    }
    
    /**
     * Zajistí řádné pozastavení simulace a případné vykonání dalších akcí,
     * například indikaci pozastavení v uživatelském rozhraní.
     */
    public abstract void simulationPaused();
    
    /**
     * {@inheritDoc}
     */
    @Override
    public final Event process() {
        simulation.logMessage(Event.SIMULATION, getTime(), "Simulace pozastavena.");
        simulationPaused();
        return null;
    }
    
}
