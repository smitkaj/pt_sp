package cz.zcu.kiv.pt.graph;

import cz.zcu.kiv.pt.simulation.SimulationTimestamp;

/**
 * Třída reprezentující uzel grafu.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class GraphNode implements INode {

    /** ID uzlu. */
    private final short id;
    /** X souřadnice. */
    private final short xPos;
    /** Y souřadnice. */
    private final short yPos;
    
    /** Cílový počet sousedů. */
    private short numbOfNeighb;
    /** Aktuální počet sousedů. */
    private short nowNumbOfNeighb;
    
    /** Čas poslední objednávky z tohoto uzlu. */
    private SimulationTimestamp lastOrderTime;

    /**
     * Konstruktor, který vytváří uzel se zadaným ID na zadaných souřadnicích v prostoru.
     * @param nodeId ID uzlu.
     * @param nodeX X souřadnice.
     * @param nodeY Y souřadnice.
     */
    public GraphNode(final short nodeId, final short nodeX, final short nodeY) {
        this.id = nodeId;
        this.xPos = nodeX;
        this.yPos = nodeY;
        this.nowNumbOfNeighb = 0;
    }
    
    /**
     * Konstruktor použitý při načítání matic ze souborů, vytvoření nových uzlů.
     * @param nodeId ID uzlu.
     */
    public GraphNode(final short nodeId) {
        this(nodeId, (short) 0, (short) 0);
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public short getId() {
        return this.id;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public short getX() {
        return this.xPos;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public short getY() {
        return this.yPos;
    }
    
    /**
     * Nastaví cílový počet sousedů.
     * @param neighbours Počet sousedů.
     */
    public void setNumbOfNeighb(final short neighbours) {
        this.numbOfNeighb = neighbours;
    }

    /**
     * Získá cílový počet sousedů.
     * @return Počet sousedů.
     */
    public short getNumbOfNeighb() {
        return this.numbOfNeighb;
    }
    
    /**
     * Nastaví aktuální počet sousedů.
     * @param nowNeighbours Počet sousedů.
     */
    public void setNowNumbOfNeighb(final short nowNeighbours) {
        this.nowNumbOfNeighb = nowNeighbours;
    }

    /**
     * Získá aktuální počet sousedů.
     * @return Počet sousedů.
     */
    public short getNowNumbOfNeighb() {
        return this.nowNumbOfNeighb;
    }

    /**
     * Získá popisku uzlu.
     * @return Popiska uzlu.
     */
    @Override
    public String toString() {
        if (id == 0) {
            return "centrála";
        } else {
            return String.valueOf(id);
        }
    }
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public SimulationTimestamp getLastOrderTime() {
        return lastOrderTime;
    }

    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setLastOrderTime(final SimulationTimestamp time) {
        lastOrderTime = time;
    }
}
