/**
 * Grafové třídy. Poskytuje třídy, které reprezentují ohodnocený neorientovaný
 * graf, jeho uzly, a třídu pro načtení a uložení grafu pomocí binárního souboru.
 */
package cz.zcu.kiv.pt.graph;