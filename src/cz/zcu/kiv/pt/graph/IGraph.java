package cz.zcu.kiv.pt.graph;

import java.util.Deque;

/**
 * Rozhraní grafu. Definuje základní operace pro získávání uzlů a jejich vzdáleností.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public interface IGraph {
    
    /**
     * Získá počet uzlů v grafu.
     * @return Počet uzlů v grafu.
     */
    short getSize();
    
    /**
     * Získá objekt určitého uzlu v grafu.
     * @param id ID uzlu.
     * @return Uzel v grafu.
     */
    INode getNode(int id);
    
    
    /**
     * Najde nejkratší cestu mezi 2 uzly.
     * @param from ID počátečního uzlu.
     * @param to ID koncového uzlu.
     * @return Délka cesty.
     */
    int getDistance(int from, int to);
    
    
    /**
     * Najde nejkratší cestu mezi 2 uzly.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     * @return Délka cesty.
     */
    int getDistance(INode from, INode to);
    
    
    /**
     * Vrátí nejkratší cestu mezi 2 uzly.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     * @return Nejkratší cesta jako posloupnost vrcholů.
     */
    Deque<INode> getPath(INode from, INode to);
    
    
    /**
     * Vrátí nejkratší cestu mezi 2 uzly.
     * @param from ID počátečního uzlu.
     * @param to ID koncového uzlu.
     * @return Nejkratší cesta jako posloupnost vrcholů.
     */
    Deque<INode> getPath(int from, int to);
}
