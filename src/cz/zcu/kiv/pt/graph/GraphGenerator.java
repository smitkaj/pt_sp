package cz.zcu.kiv.pt.graph;

import java.util.Random;

/**
 * Generátor grafu.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class GraphGenerator {

    /** Šířka grafu. */
    private final short worldSize;
    /** Hustota grafu. */
    private final short density;
    /** Instance třídy random. */
    private final Random rand = new Random();
    
    /**
     * Vytvoří generátor grafu pro zadané parametry.
     * @param gWorldSize Velikost světa.
     * @param gDensity Hustota grafu.
     */
    public GraphGenerator(final short gWorldSize, final short gDensity) {
        this.worldSize = gWorldSize;
        this.density = gDensity;
    }

    /**
     * Vygeneruje uzly a uloží je do pole.
     * @param graph Graf, do kterého budou uzly vloženy.
     */
    public void generateNodes(final Graph graph) {
        short xPos;
        short yPos;
        boolean same = false;
        short size = graph.getSize();
        graph.addNode(new GraphNode((short) 0, (short) (this.worldSize / 2), (short) (this.worldSize / 2))); //továrna uprostřed grafu (id je 0)
        for (short i = 1; i < size; i++) {
            xPos = (short) rand.nextInt(this.worldSize + 1); //od nuly až do rozsahu
            yPos = (short) rand.nextInt(this.worldSize + 1);
            for (short j = 0; j < i; j++) {
                if (graph.getNode(j).getX() == xPos && graph.getNode(j).getY() == yPos) {
                    same = true;
                    break;
                }
            }
            if (same) {
                i--;
                same = false;
            } else {
                graph.addNode(new GraphNode(i, xPos, yPos));
            }
        }
    }

    /**
     * Počítá vzdálenosti mezi dvěma body.
     * @param x1 x-ová souřadnice výchozího bodu
     * @param y1 y-ová souřadnice výchozího bodu
     * @param x2 x-ová souřadnice koncového bodu
     * @param y2 y-ová souřadnice koncového bodu
     * @return vzdálenost výchozího a koncového bodu
     */
    public short countDistance(final short x1, final short y1, final short x2, final short y2) {
        if (x1 == x2) {
            return (short) Math.abs(y2 - y1);
        } else if (y1 == y2) {
            return (short) Math.abs(x2 - x1);
        } else {
            return (short) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        }

    }

    /**
     * Vygeneruje nový graf o zadaném počtu uzlů.
     * @param size Velikost grafu.
     * @return Nový graf.
     */
    public Graph generateGraph(final short size) {
        Graph graph = new Graph(size, worldSize);
        
        generateNodes(graph);
        short neighbId;        
        short topBound;
        
        for (int i = 0; i < size; i++) {
            GraphNode node = (GraphNode) graph.getNode(i);
            node.setNumbOfNeighb((short) (rand.nextInt(this.density) + 1));  //nastavení počtu sousedů uzlu
            
            if (i < size - 1) {    // id = 0 až předposlední
                //vzdálenost uzlu od následujícího uzlu (aby byl graf souvislý)
                graph.setDistance(i, i + 1, countDistance(node.getX(), node.getY(), graph.getNode(i + 1).getX(), graph.getNode(0).getY()));
            } else {
                //poslední uzel se spojí s prvním(id = 0, továrna)
                graph.setDistance(i, 0, countDistance(node.getX(), node.getY(), graph.getNode(0).getX(), graph.getNode(0).getY()));
            }
            
            for (int k = 1; k < size; k++) {
                if (k == 1 || k == size) {
                    ((GraphNode) graph.getNode(k)).setNowNumbOfNeighb((short) 1); //sousedí zatím pouze s jedním zmrzlinářstvím
                } else {
                    ((GraphNode) graph.getNode(k)).setNowNumbOfNeighb((short) 2); //sousedí zatím pouze s dvěma zmrzlinářstvími
                }
            }
            
            topBound = (short) (node.getNumbOfNeighb() - node.getNowNumbOfNeighb());
            for (int j = 0; j < topBound; j++) {    //cyklus pro počítání vzdálenosti sousedů od stávajícího uzlu (počet sousedů-1 následující)
                neighbId = (short) (rand.nextInt(size - 1) + 1); //náhodný soused (od 1 do počtu uzlů-1:kvůli id)
                if (graph.getDistance(i, neighbId) == Short.MAX_VALUE) {  //ověření, že ještě sousedem není
                    GraphNode neighbour = (GraphNode) graph.getNode(neighbId);
                    graph.setDistance(i, neighbId, countDistance(node.getX(), node.getY(), neighbour.getX(), neighbour.getY()));
                    neighbour.setNowNumbOfNeighb((short) (neighbour.getNowNumbOfNeighb() + 1)); //po každém přidání souseda se přičte do proměnné stávajících sousedů toho souseda
                } else {    //pokud už sousedem byl, budu vybírat znovu
                    j--;
                }
            }
        }
        
        return graph;
    }
}
