/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.graph;

/**
 * Výjimka indikující chybná data ve vstupním souboru.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public class GraphLoaderException extends Exception {
    /** Serializační ID. */
    private static final long serialVersionUID = 5454989566L;

    /**
     * Vytvoří výjimku se zadanou zprávou.
     * @param message Chybová zpráva.
     */
    public GraphLoaderException(final String message) {
        super(message);
    }

    /**
     * Vytvoří prázdnou výjimku.
     */
    public GraphLoaderException() {
        // prázdná výjimka
    }
}
