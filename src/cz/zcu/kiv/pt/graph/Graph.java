package cz.zcu.kiv.pt.graph;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Deque;
import java.util.LinkedList;


/**
 * Třída reprezentující graf.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class Graph implements IGraph {
    /** Jak často předávat hlášení o stavu výpočtu cest. */
    private static final int REPORT_FREQ = 10;
    
    /** Velikost grafu. */
    private final short size;
    
    /** Velikost světa, který byl použit pro vygenerování grafu. */
    private final short worldSize;
    
    /** Uzly grafu. */
    private GraphNode[] nodes;

    /** Distanční matice. */
    private short[][] distMatrix;
    
    /** Matice cest. */
    private short[][] pathMatrix;
    
    /** Událost na hlášení stavu průbehu hledání cest. */
    private ActionListener statusListener;
    
    
    /**
     * Vytvoří nový graf o zadané velikosti.
     * @param gSize Počet uzlů grafu.
     * @param gWorldSize Velikost plochy, do kterého budou vkládány jednotlivé uzly.
     */
    public Graph(final short gSize, final short gWorldSize) {
        this.size = gSize;
        this.worldSize = gWorldSize;
        this.nodes = new GraphNode[size];
        
        distMatrix = new short[size][size];
        for (short i = 0; i < size; i++) {
            for (short j = 0; j < size; j++) {
                if (i != j) {
                    distMatrix[i][j] = Short.MAX_VALUE;
                }
            }
        }
        pathMatrix = new short[size][size];
        for (short i = 0; i < size; i++) {          //inicializace matice předchůdců
            for (short j = 0; j < size; j++) {
                pathMatrix[i][j] = -1;
            }
        }
    }
    
    
    /**
     * Vytvoří graf ze zadaných uzlů, distanční matice a matice předchůdců.
     * @param gWorldSize Velikost plochy, ve kterém byly vytvořeny jednotlivé uzly.
     * @param gNodes Seznam uzlů.
     * @param gDistMatrix Distanční matice.
     * @param gPathMatrix Matice předchůdců.
     */
    public Graph(final short gWorldSize, final GraphNode[] gNodes, final short[][] gDistMatrix, final short[][] gPathMatrix) {
        this.size = (short) gNodes.length;
        this.nodes = gNodes;
        this.worldSize = gWorldSize;
        this.distMatrix = gDistMatrix;
        this.pathMatrix = gPathMatrix;
    }
    
    /**
     * Nastaví událost, která hlásí informace o procesu generování.
     * @param listener Listener události.
     */
    public void setStatusListener(final ActionListener listener) {
        this.statusListener = listener;
    }
    
    /**
     * Získá pole uzlů grafu.
     * @return Pole uzlů.
     */
    public GraphNode[] getNodes() {
        return nodes;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public INode getNode(final int id) {
        return nodes[id];
    }
    
    
    /**
     * Vloží uzel do grafu. Pokud již existuje uzel se zadaným ID, bude přepsán.
     * @param node Uzel grafu s vyplněným ID.
     */
    public void addNode(final GraphNode node) {
        nodes[node.getId()] = node;
    }
    
    
    /**
     * Provede výpočet cest. Používá algoritmus floyd-warshall.
     */
    public void floydWarshall() {
        //Floyd-Warshall
        for (short k = 0; k < size; k++) {
            for (short i = 0; i < size; i++) {
                if(i != k) {
                    for (short j = 0; j < i; j++) {

                        if (distMatrix[i][k] == Short.MAX_VALUE || distMatrix[k][j] == Short.MAX_VALUE || j==k) {
                            continue;
                        }
                        if (distMatrix[i][k] + distMatrix[k][j] <= distMatrix[i][j]) {
                            distMatrix[i][j] = (short) (distMatrix[i][k] + distMatrix[k][j]);
                            distMatrix[j][i] = distMatrix[i][j];
                            this.pathMatrix[i][j] = k;
                            this.pathMatrix[j][i] = k;
                        }
                    }
                }
            }
            if (k % REPORT_FREQ == 0 && statusListener != null) {
                statusListener.actionPerformed(new ActionEvent(this, k, "progress"));
            }
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public short getSize() {
        return size;
    }
    
    /**
     * Získá velikost světa.
     * @return Velikost světa.
     */
    public short getWorldSize() {
        return worldSize;
    }

    /**
     * Získá distanční matici.
     * @return Distanční matice.
     */
    public short[][] getDistMatrix() {
        return distMatrix;
    }

    /**
     * Získá matici cest.
     * @return Matice cest.
     */
    public short[][] getPathMatrix() {
        return pathMatrix;
    }
    

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDistance(final int from, final int to) {
        return distMatrix[from][to];
    }
    
    
    /**
     * Nastaví v distanční matici cestu mezi dvěma uzly. Matice je vždy symetrická,
     * takže je možné počáteční a koncový uzel prohodit.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     * @param distance Vzdálenost.
     */
    public void setDistance(final int from, final int to, final short distance) {
        distMatrix[from][to] = distance;
        distMatrix[to][from] = distance;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDistance(final INode from, final INode to) {
        return getDistance(from.getId(), to.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Deque<INode> getPath(final INode from, final INode to) {
        return getPath(from.getId(), to.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Deque<INode> getPath(final int from, final int to) {
        Deque<INode> path = new LinkedList<INode>();
        
        path.add(nodes[from]);
        buildPath(path, from, to);
        path.add(nodes[to]);
        
        return path;
    }
    
    /**
     * Interní metoda pro sestavování cesty mezi 2 uzly.
     * @param path Cesta.
     * @param from Počáteční uzel.
     * @param to Koncový uzel.
     */
    private void buildPath(final Deque<INode> path, final int from, final int to) {
        short over = pathMatrix[from][to];
        if (over != -1) {
            buildPath(path, from, over);
            path.add(nodes[over]);
            buildPath(path, over, to);
        }
    }
    
    /**
     * Získá sousedy uzlu se zadaným ID.
     * @param id ID uzlu.
     * @return Seznam sousedů.
     */
    public Deque<GraphNode> getNeighbours(final short id) {
        Deque<GraphNode> neighbours = new LinkedList<GraphNode>();
        for (int i = 0; i < size; i++) {
            if (i != id && distMatrix[id][i] < Short.MAX_VALUE && pathMatrix[id][i] == -1) {
                neighbours.add(nodes[i]);
            }
        }
        return neighbours;
    } 
    
    /**
     * Získá sousedy zadaného uzlu.
     * @param node Uzel grafu.
     * @return Seznam sousedů.
     */
    public Deque<GraphNode> getNeighbours(final GraphNode node) {
        return getNeighbours(node.getId());
    }
}
