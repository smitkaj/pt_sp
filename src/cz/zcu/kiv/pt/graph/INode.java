package cz.zcu.kiv.pt.graph;

import cz.zcu.kiv.pt.simulation.SimulationTimestamp;

/**
 * Interface uzlu grafu. Definuje operace pro získání identifikátoru uzlu,
 * uchovávání data poslední objednávky a získávání souřadnic uzlu.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public interface INode {
    /**
     * Získá ID uzlu grafu.
     * @return ID uzlu.
     */
    short getId();
    
    /**
     * Získá popisku uzlu.
     * @return Popiska.
     */
    @Override
    String toString();
    
    /**
     * Získá čas poslední objednávky.
     * @return Čas poslední objednávky.
     */
    SimulationTimestamp getLastOrderTime();
    
    /**
     * Nastaví čas, kdy zmrzlinářství naposledy objednávalo.
     * @param time Čas poslední objednávky.
     */
    void setLastOrderTime(SimulationTimestamp time);
    
    /**
     * Získá X souřadnici uzlu.
     * @return X souřadnice.
     */
    short getX();
    
    /**
     * Získá Y souřadnici uzlu.
     * @return Y souřadnice.
     */
    short getY();
}
