package cz.zcu.kiv.pt.graph;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Statická třída, která zajišťuje ukládání a načítání grafu. Pro uložení je
 * použit binární soubor.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class GraphLoader {
    /** Hlavička souboru. */
    private static final String FILE_HEADER = "PTGRAPH";
    /** Aktuální verze souboru. */
    private static final byte FILE_VERSION = 3;
    
    /** Velikost bufferu pro čtení. */
    private static final int BUFFER_SIZE = 64 * 1024;
    
    /**
     * Statická factory třída, nelze instancovat.
     */
    private GraphLoader() {
        
    }
    
    
    /**
     * Načte graf ze souboru.
     * @param filename Název souboru.
     * @return Načtený graf.
     * @throws IOException Pokud dojde k chybě při čtení souboru.
     * @throws GraphLoaderException Pokud soubor obsahuje neplatná data.
     */
    public static Graph load(final String filename) throws IOException, GraphLoaderException {
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(filename), BUFFER_SIZE));
        Graph graph;
        try {
            byte[] header = new byte[FILE_HEADER.length()];
            int readBytes = dis.read(header, 0, FILE_HEADER.length());
            if (readBytes != FILE_HEADER.length() || !Arrays.equals(header, FILE_HEADER.getBytes())) {
                throw new GraphLoaderException("Soubor obsahuje špatnou hlavičku.");
            }
            if (dis.readByte() != FILE_VERSION) {
                throw new GraphLoaderException("Soubor má nesprávnou verzi.");
            }
            short size = dis.readShort();
            short worldSize = dis.readShort();

            GraphNode[] nodes = new GraphNode[size];
            for (short i = 0; i < size; i++) {
                nodes[i] = new GraphNode(i, dis.readShort(), dis.readShort());
            }
            
            short[][] distMatrix = loadMatrix(dis, size);
            short[][] pathMatrix = loadMatrix(dis, size);
            
            graph = new Graph(worldSize, nodes, distMatrix, pathMatrix);
        } catch (IOException e) {
            throw e;
        } catch (GraphLoaderException e) {
            throw e;
        } finally {
            dis.close();
        }
        
        return graph;
    }
    
    
    /**
     * Uloží graf do souboru.
     * @param graph Graf k uložení.
     * @param filename Název výstupního souboru.
     * @throws IOException Pokud dojde k chybě při ukládání souboru.
     */
    public static void save(Graph graph, final String filename) throws IOException {
        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filename), BUFFER_SIZE));
        try {
            dos.writeBytes(FILE_HEADER);
            dos.writeByte(FILE_VERSION);
            dos.writeShort(graph.getSize());
            dos.writeShort(graph.getWorldSize());
            
            for (GraphNode node : graph.getNodes()) {
                dos.writeShort(node.getX());
                dos.writeShort(node.getY());
            }
            
            saveMatrix(graph.getDistMatrix(), dos);
            saveMatrix(graph.getPathMatrix(), dos);
        } catch (IOException e) {
            throw e;
        } finally {
            dos.close();
        }
    }
    

    /**
     * Načte matici ze souboru.
     * @param dis Vstupní proud.
     * @param numberOfNodes Počet uzlů grafu.
     * @return Načtená matice.
     * @throws IOException V případě, že dojde k chybě při čtení.
     */
    private static short[][] loadMatrix(DataInputStream dis, short numberOfNodes) throws IOException {
        short[][] matrix = new short[numberOfNodes][numberOfNodes];
        
        for (short i = 0; i < numberOfNodes; i++) {
            for (short j = 0; j < numberOfNodes; j++) {
                matrix[i][j] = dis.readShort();
            }
        }
        return matrix;
    }
    

    /**
     * 
     * @param matrix
     * @param dos
     * @throws IOException V případě, že dojde k chybě při zápisu.
     */
    private static void saveMatrix(final short[][] matrix, DataOutputStream dos) throws IOException {
        for (short i = 0; i < matrix.length; i++) {
            for (short j = 0; j < matrix.length; j++) {
                dos.writeShort(matrix[i][j]);
            }
        }
    }
}
