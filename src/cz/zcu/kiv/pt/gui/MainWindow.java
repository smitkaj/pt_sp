package cz.zcu.kiv.pt.gui;

import cz.zcu.kiv.pt.simulation.loggers.TableLogger;
import cz.zcu.kiv.pt.graph.Graph;
import cz.zcu.kiv.pt.graph.GraphLoader;
import cz.zcu.kiv.pt.graph.GraphLoaderException;
import cz.zcu.kiv.pt.simulation.Event;
import cz.zcu.kiv.pt.simulation.NodeStatistics;
import cz.zcu.kiv.pt.simulation.Route;
import cz.zcu.kiv.pt.simulation.Simulation;
import cz.zcu.kiv.pt.simulation.SimulationTimestamp;
import cz.zcu.kiv.pt.simulation.StatisticsCollector;
import cz.zcu.kiv.pt.simulation.loggers.FileLogger;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 * Hlavní okno uživatelského rozhraní.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class MainWindow extends javax.swing.JFrame {
    /** Serializační ID. */
    private static final long serialVersionUID = 787421315L;
    
    /** Graf. */
    private transient Graph graph;
    /** Vlákno se simulací. */
    private transient SimulationThread simulationThread;
    /** Seznam breakpointů. */
    private DefaultListModel breakpointsModel;

    /**
     * Vytvoří nové hlavní okno.
     */
    public MainWindow() {
        initComponents();
        
        logTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        logTable.getColumnModel().getColumn(0).setMaxWidth(100);
        
        routeTable.getColumnModel().getColumn(0).setMaxWidth(30);
        
        setLocationRelativeTo(null);
    }
    
    /**
     * Vytvoří novou simulaci.
     * @param days Počet dnů k simulování.
     * @return Nová simulace.
     */
    private Simulation createSimulation(final byte days) {
        Simulation simulation = new Simulation(graph, days);
        simulation.attachLogger(new TableLogger(logTable));
        for (int i = 0; i < breakpointsModel.getSize(); i++) {
            simulation.addBreakpoint(new GuiBreakpoint(simulation, (SimulationTimestamp) breakpointsModel.get(i), this));
        }
        return simulation;
    }
    
    /**
     * Vytvoří prázdný seznam breakpointů.
     * @return Prázdný model.
     */
    private ListModel createBreakpointListModel() {
        breakpointsModel = new DefaultListModel();
        return breakpointsModel;
    }
    
    /**
     * Zajistí zablokování prvků pro řízení simulace. Voláno při spuštění simulace.
     */
    public void simulationRunning() {
        simluationControlsSetEnabled(false);
        statusLabel.setText("Simulace běží.");
    }
    
    
    /**
     * Pomocná metoda na odscrollování tabulky s výpisem simulace na konec.
     */
    private void scrollLogTable() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                logTable.scrollRectToVisible(logTable.getCellRect(logTable.getRowCount() - 1, 0, true));
            }
        });
    }
    
    /**
     * Zajistí povolení prvků pro řízení simulace. Voláno při pozastavení simulace.
     * @param breakpoint Breakpoint, který pozastavení vyvolal.
     */
    public void simulationPaused(final GuiBreakpoint breakpoint) {
        breakpointsModel.removeElement(breakpoint.getTime());        
        statusLabel.setText(String.format("Simulace pozastavena v %s", breakpoint.getTime()));
        simluationControlsSetEnabled(true);
        Simulation simulation = simulationThread.getSimulation();
        DefaultTableModel model = (DefaultTableModel) routeTable.getModel();
        model.setNumRows(0);
        for (Event event : simulation.getEvents()) {
            if (event instanceof Route) {
                Route route = (Route) event;
                if (route.getId() != 0) {
                    model.addRow(new Object[] {route.getId(), route});
                }
            }
        }
        
        model = (DefaultTableModel) nodeTable.getModel();
        model.setNumRows(0);
        for (NodeStatistics node : simulation.getStatistics().getNodes().values()) {
            model.addRow(new Object[] {node.getNodeId(), node.getCargoTotal(), node.getCargoDelivered()});
        }
        
        scrollLogTable();
    }
    
    /**
     * Zobrazí uživateli chybovou hlášku ze simulace.
     * @param exception Výjimka.
     */
    public void simulationError(final Exception exception) {
        StringBuilder message = new StringBuilder();
        message.append(exception.toString());
        message.append("\n");
        for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
            message.append(stackTraceElement.toString());
            message.append("\n");
        }
        
        JOptionPane.showMessageDialog(this, message.toString(),
                "Chyba při vykonávání simulace", JOptionPane.ERROR_MESSAGE);
        
        scrollLogTable();
    }
    
    
    /**
     * Při ukončení simulace provede patřičné změny v uživatelském rozhraní a zajistí vygenerování statistik.
     */
    public void simulationFinished() {
        statusLabel.setText("Simulace ukončena.");
        
        scrollLogTable();
        
        JFileChooser chooser = new JFileChooser("./");
        chooser.setDialogTitle("Vyberte soubor, do kterého chcete uložit statistiky");
        chooser.setFileFilter(new FileNameExtensionFilter("HTML soubory (*.htm, *.html)", "html", "htm"));
        if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                this.getSimulationThread().getSimulation().getStatistics().generateStats(chooser.getSelectedFile().getAbsolutePath());
                Desktop.getDesktop().browse(chooser.getSelectedFile().toURI());
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Během zápisu do souboru došlo k chybě.", "Chyba", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        toolsTabPane.setEnabled(true);
        addBreakpointButton.setEnabled(true);
        removeBreakpointButton.setEnabled(true);
    }
    
    
    /**
     * Povolí či zablokuje prvky pro řízení simulace.
     * @param enabled Jsou prvky povoleny?
     */
    private void simluationControlsSetEnabled(final boolean enabled) {
        toolsTabPane.setEnabled(enabled);
        continueButton.setEnabled(enabled);
        addOrderButton.setEnabled(enabled);
        
        addBreakpointButton.setEnabled(enabled);
        removeBreakpointButton.setEnabled(enabled);
    }
    
    /**
     * Získá vlákno se simulací.
     * @return Vlákno se simulací.
     */
    public SimulationThread getSimulationThread() {
        return simulationThread;
    }
    
    /**
     * Nastaví graf, se kterým bude simulace pracovat.
     * @param newGraph Nový graf.
     */
    public void setGraph(final Graph newGraph) {
        graph = newGraph;
        worldMap.setGraph(graph);
        worldMap.setPreferredSize(new Dimension(graph.getWorldSize(), graph.getWorldSize()));
        startSimulationButton.setEnabled(true);
    }
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        toolBar = new javax.swing.JToolBar();
        loadGraphButton = new javax.swing.JButton();
        genGraphButton = new javax.swing.JButton();
        separator1 = new javax.swing.JToolBar.Separator();
        startSimulationButton = new javax.swing.JButton();
        separator2 = new javax.swing.JToolBar.Separator();
        continueButton = new javax.swing.JButton();
        addOrderButton = new javax.swing.JButton();
        middleFiller = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        statusLabel = new javax.swing.JLabel();
        rightMarginFiller = new javax.swing.Box.Filler(new java.awt.Dimension(16, 0), new java.awt.Dimension(16, 0), new java.awt.Dimension(16, 32767));
        mainTabPane = new javax.swing.JTabbedPane();
        logPanel = new javax.swing.JPanel();
        logScrollPane = new javax.swing.JScrollPane();
        logTable = new javax.swing.JTable();
        mapScrollPane = new javax.swing.JScrollPane();
        worldMap = new cz.zcu.kiv.pt.gui.VisualizationPanel();
        toolsTabPane = new javax.swing.JTabbedPane();
        breakpointPanel = new javax.swing.JPanel();
        breakpointScrollPane = new javax.swing.JScrollPane();
        breakpointList = new javax.swing.JList();
        breakpointToolbar = new javax.swing.JToolBar();
        addBreakpointButton = new javax.swing.JButton();
        removeBreakpointButton = new javax.swing.JButton();
        routePanel = new javax.swing.JPanel();
        routeScrollPane = new javax.swing.JScrollPane();
        routeTable = new javax.swing.JTable();
        nodePanel = new javax.swing.JPanel();
        nodeScrollPane = new javax.swing.JScrollPane();
        nodeTable = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Zmrzlík - Semestrální práce KIV/PT");
        setIconImage(Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("resources/lorry.png")));

        toolBar.setFloatable(false);
        toolBar.setRollover(true);

        loadGraphButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/map.png"))); // NOI18N
        loadGraphButton.setText("Načíst graf");
        loadGraphButton.setFocusable(false);
        loadGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        loadGraphButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                loadGraphButtonActionPerformed(evt);
            }
        });
        toolBar.add(loadGraphButton);

        genGraphButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/map_go.png"))); // NOI18N
        genGraphButton.setText("Vygenerovat graf");
        genGraphButton.setFocusable(false);
        genGraphButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        genGraphButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                genGraphButtonActionPerformed(evt);
            }
        });
        toolBar.add(genGraphButton);
        toolBar.add(separator1);

        startSimulationButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/lorry_go.png"))); // NOI18N
        startSimulationButton.setText("Spustit simulaci");
        startSimulationButton.setEnabled(false);
        startSimulationButton.setFocusable(false);
        startSimulationButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        startSimulationButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                startSimulationButtonActionPerformed(evt);
            }
        });
        toolBar.add(startSimulationButton);
        toolBar.add(separator2);

        continueButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/flag_1.png"))); // NOI18N
        continueButton.setText("Pokračovat v simulaci");
        continueButton.setEnabled(false);
        continueButton.setFocusable(false);
        continueButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        continueButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                continueButtonActionPerformed(evt);
            }
        });
        toolBar.add(continueButton);

        addOrderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/page_add.png"))); // NOI18N
        addOrderButton.setText("Zadat objednávku");
        addOrderButton.setEnabled(false);
        addOrderButton.setFocusable(false);
        addOrderButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        addOrderButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        addOrderButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                addOrderButtonActionPerformed(evt);
            }
        });
        toolBar.add(addOrderButton);
        toolBar.add(middleFiller);
        toolBar.add(statusLabel);
        toolBar.add(rightMarginFiller);

        logTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Čas", "Zpráva"
            }
        ) {
            /** Serializační ID. */
            private static final long serialVersionUID = 2135461546L;
            
            private final Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class
            };
            private final boolean[] canEdit = new boolean [] {
                false, false
            };

            @Override
            public Class getColumnClass(final int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(final int rowIndex, final int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        logTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        logTable.setShowVerticalLines(false);
        logScrollPane.setViewportView(logTable);

        javax.swing.GroupLayout logPanelLayout = new javax.swing.GroupLayout(logPanel);
        logPanel.setLayout(logPanelLayout);
        logPanelLayout.setHorizontalGroup(
            logPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(logScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
        );
        logPanelLayout.setVerticalGroup(
            logPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(logScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
        );

        mainTabPane.addTab("Simulace", new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/scroller_bar.png")), logPanel); // NOI18N

        worldMap.setDoubleBuffered(false);

        javax.swing.GroupLayout worldMapLayout = new javax.swing.GroupLayout(worldMap);
        worldMap.setLayout(worldMapLayout);
        worldMapLayout.setHorizontalGroup(
            worldMapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 687, Short.MAX_VALUE)
        );
        worldMapLayout.setVerticalGroup(
            worldMapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 592, Short.MAX_VALUE)
        );

        mapScrollPane.setViewportView(worldMap);

        mainTabPane.addTab("Vizualizace", new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/map.png")), mapScrollPane); // NOI18N

        breakpointList.setModel(createBreakpointListModel());
        breakpointList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            @Override
            public void valueChanged(final javax.swing.event.ListSelectionEvent evt) {
                listValueChanged(evt);
            }
        });
        breakpointScrollPane.setViewportView(breakpointList);

        breakpointToolbar.setFloatable(false);
        breakpointToolbar.setRollover(true);

        addBreakpointButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/add.png"))); // NOI18N
        addBreakpointButton.setText("Přidat");
        addBreakpointButton.setFocusable(false);
        addBreakpointButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        addBreakpointButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                addBreakpointButtonActionPerformed(evt);
            }
        });
        breakpointToolbar.add(addBreakpointButton);

        removeBreakpointButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/delete.png"))); // NOI18N
        removeBreakpointButton.setText("Odebrat");
        removeBreakpointButton.setEnabled(false);
        removeBreakpointButton.setFocusable(false);
        removeBreakpointButton.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        removeBreakpointButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                removeBreakpointButtonActionPerformed(evt);
            }
        });
        breakpointToolbar.add(removeBreakpointButton);

        javax.swing.GroupLayout breakpointPanelLayout = new javax.swing.GroupLayout(breakpointPanel);
        breakpointPanel.setLayout(breakpointPanelLayout);
        breakpointPanelLayout.setHorizontalGroup(
            breakpointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(breakpointScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
            .addComponent(breakpointToolbar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        breakpointPanelLayout.setVerticalGroup(
            breakpointPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, breakpointPanelLayout.createSequentialGroup()
                .addComponent(breakpointToolbar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(breakpointScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 563, Short.MAX_VALUE))
        );

        toolsTabPane.addTab("Breakpointy", new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/stop.png")), breakpointPanel); // NOI18N

        routeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Akce"
            }
        ) {
            /** Serializační ID. */
            private static final long serialVersionUID = 18456464335L;
            
            private final Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class
            };
            private final boolean[] canEdit = new boolean [] {
                false, false
            };

            @Override
            public Class getColumnClass(final int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(final int rowIndex, final int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        routeTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        routeTable.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(final java.awt.event.MouseEvent evt) {
                routeTableMouseClicked(evt);
            }
        });
        routeScrollPane.setViewportView(routeTable);

        javax.swing.GroupLayout routePanelLayout = new javax.swing.GroupLayout(routePanel);
        routePanel.setLayout(routePanelLayout);
        routePanelLayout.setHorizontalGroup(
            routePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(routeScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
        );
        routePanelLayout.setVerticalGroup(
            routePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(routeScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
        );

        toolsTabPane.addTab("Vozy", new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/lorry.png")), routePanel); // NOI18N

        nodeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Objednáno", "Doručeno"
            }
        ) {
            /** Serializační ID. */
            private static final long serialVersionUID = 65456321315L;
            
            private Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            private boolean[] canEdit = new boolean [] {
                false, false, false
            };

            @Override
            public Class getColumnClass(final int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(final int rowIndex, final int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        nodeTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_LAST_COLUMN);
        nodeTable.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(final java.awt.event.MouseEvent evt) {
                nodeTableMouseClicked(evt);
            }
        });
        nodeScrollPane.setViewportView(nodeTable);

        javax.swing.GroupLayout nodePanelLayout = new javax.swing.GroupLayout(nodePanel);
        nodePanel.setLayout(nodePanelLayout);
        nodePanelLayout.setHorizontalGroup(
            nodePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nodeScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
        );
        nodePanelLayout.setVerticalGroup(
            nodePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(nodeScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
        );

        toolsTabPane.addTab("Zmrzlinářství", new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/building.png")), nodePanel); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainTabPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(toolsTabPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mainTabPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(toolsTabPane))
                .addContainerGap())
        );

        pack();
    }

    /**
     * Akce při stisknutí tlačítka pro načtení grafu.
     * @param evt Událost.
     */
    private void loadGraphButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        JFileChooser chooser = new JFileChooser("./");
        chooser.setDialogTitle("Načíst graf");
        chooser.setFileFilter(new FileNameExtensionFilter("Binární soubory grafu (*.bin)", "bin"));
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                setGraph(GraphLoader.load(chooser.getSelectedFile().getAbsolutePath()));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Chyba při otevírání souboru.");
            } catch (ArrayIndexOutOfBoundsException ex) {
                JOptionPane.showMessageDialog(this, "Neplatný formát vstupních dat.");
            } catch (GraphLoaderException ex) {
                JOptionPane.showMessageDialog(this, "Nelze načíst soubor: " + ex.getMessage());
            }
        }
    }

    
    /**
     * Akce při stisku tlačítka pro spuštění simulace.
     * @param evt Událost.
     */
    private void startSimulationButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        StartSimulationDialog window = new StartSimulationDialog(this);
        window.setVisible(true);
        if (window.getResult()) {
            ((DefaultTableModel) logTable.getModel()).setRowCount(0);
            Simulation simulation = createSimulation(window.getDays());
            simulation.generateOrders(window.getOrders());
            if (window.isUseFile()) {
                try {
                    simulation.attachLogger(new FileLogger(window.getFilename()));
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), "Chyba při vytváření souboru", JOptionPane.ERROR_MESSAGE);
                }
            }
            simulationRunning();
            simulationThread = new SimulationThread(simulation);
            simulationThread.setErrorListener(new ActionListener() {
                /**
                 * Předá zpracování chyby metodě {@link MainWindow#simulationError}.
                 * @param evt Informace o chybě.
                 */
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    simulationError((Exception) evt.getSource());
                }
            });
            simulationThread.setFinishedListener(new ActionListener() {
                /**
                 * Předá zpracování řádného ukončení simulace metodě {@link MainWindow#simulationFinished}.
                 * @param e Informace u události.
                 */
                @Override
                public void actionPerformed(final ActionEvent evt) {
                    simulationFinished();
                }
            });
            simulationThread.start();
        }
    }

    
    /**
     * Akce při stisku tlačítka pro přidání breakpointu.
     * @param evt Událost.
     */
    private void addBreakpointButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        SelectTimeDialog dialog = new SelectTimeDialog(this);
        dialog.setVisible(true);
        if (dialog.getResult()) {
            SimulationTimestamp time = dialog.getTime();
            if (simulationThread == null || !simulationThread.isRunning()) {
                breakpointsModel.addElement(time);
            } else {
                Simulation sim = simulationThread.getSimulation();
                SimulationTimestamp currentTime = sim.getCurrentTime();
                SimulationTimestamp endTime = new SimulationTimestamp(sim.getSimulationEnd());
                if (time.compareTo(currentTime) <= 0) {
                    JOptionPane.showMessageDialog(this, "Nelze nastavit breakpoint v minulosti.", "Neplatný čas", JOptionPane.ERROR_MESSAGE);
                } else if (time.compareTo(endTime) > 0) {
                    JOptionPane.showMessageDialog(this, "Nelze nastavit breakpoint po konci simulace.", "Neplatný čas", JOptionPane.ERROR_MESSAGE);
                } else {
                    breakpointsModel.addElement(time);
                    simulationThread.getSimulation().addBreakpoint(new GuiBreakpoint(simulationThread.getSimulation(), time, this));
                }
            }
        }
    }

    
    /**
     * Akce při stisku tlačítka pro odebrání breakpointu.
     * @param evt Událost.
     */
    @SuppressWarnings("deprecation")
    private void removeBreakpointButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        for (Object object : breakpointList.getSelectedValues()) {
            breakpointsModel.removeElement(object);
        }
    }

    
    /**
     * Akce při stisku tlačítka pro pokračování simulace.
     * @param evt Událost.
     */
    private void continueButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        simulationRunning();
        synchronized (simulationThread) {
            simulationThread.setPaused(false);
            simulationThread.notifyAll();
        }
    }

    /**
     * Akce při výběru breakpointu v seznamu.
     * @param evt Událost.
     */
    private void listValueChanged(final javax.swing.event.ListSelectionEvent evt) {
        removeBreakpointButton.setEnabled(breakpointList.getSelectedIndices().length > 0);
    }

    /**
     * Akce při stisku tlačítka pro vygenerování grafu.
     * @param evt Událost.
     */
    private void genGraphButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        GenerateDialog dialog = new GenerateDialog(this);
        dialog.setVisible(true);
        
        if (dialog.getGraph() != null) {
            setGraph(dialog.getGraph());
        }
    }

    
    /**
     * Akce při výběru výjezdu v tabulce aktuálních výjezdů.
     * @param evt Událost.
     */
    private void routeTableMouseClicked(final java.awt.event.MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON1) {
            int row = routeTable.rowAtPoint(evt.getPoint());
                Route route = (Route) routeTable.getValueAt(row, 1);
            if (evt.getClickCount() == 2) {
                RouteInfoDialog dialog = new RouteInfoDialog(this, route);
                dialog.setVisible(true);
            } else if (evt.getClickCount() == 1 && mainTabPane.getSelectedIndex() == 1) {
                worldMap.highlightRoute(route);
            }
        }
    }

    
    /**
     * Zpracování události kliknutí na pobočku v seznamu poboček..
     * @param evt Událost.
     */
    private void nodeTableMouseClicked(final java.awt.event.MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON1) {
            int row = nodeTable.rowAtPoint(evt.getPoint());
            StatisticsCollector stats = simulationThread.getSimulation().getStatistics();
            NodeStatistics node =  stats.getNodes().get((Short) nodeTable.getValueAt(row, 0));
            if (evt.getClickCount() == 2) {
                NodeInfoDialog dialog = new NodeInfoDialog(this, node);
                dialog.setVisible(true);
            } else if (evt.getClickCount() == 1 && mainTabPane.getSelectedIndex() == 1) {
                worldMap.highlightNode(graph.getNode(node.getNodeId()));
            }
        }
    }

    
    /**
     * Zpracování události po stisku tlačítka na vytvoření objednávky.
     * @param evt Událost.
     */
    private void addOrderButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        NewOrderDialog dialog = new NewOrderDialog(this, simulationThread.getSimulation());
        dialog.setVisible(true);
        
        simulationThread.getSimulation().addOrder(dialog.getOrder());
    }

    /**
     * Metoda main.
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            /*for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {    
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }*/
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }
    
    /** Tlačítko pro přidání breakpointu. */
    private javax.swing.JButton addBreakpointButton;
    /** Tlačítko pro přidání objednávky. */
    private javax.swing.JButton addOrderButton;
    /** Seznam breakpointů. */
    private javax.swing.JList breakpointList;
    /** Panel se seznamem breakpointů. */
    private javax.swing.JPanel breakpointPanel;
    /** Scrollpane breakpointů. */
    private javax.swing.JScrollPane breakpointScrollPane;
    /** Toolbar pro práci s breakpointy. */
    private javax.swing.JToolBar breakpointToolbar;
    /** Tlačítko pro pokračování simulace. */
    private javax.swing.JButton continueButton;
    /** Tlačítko pro vygenerování nového grafu. */
    private javax.swing.JButton genGraphButton;
    /** Tlačítko pro načtení grafu. */
    private javax.swing.JButton loadGraphButton;
    /** Log panel. */
    private javax.swing.JPanel logPanel;
    /** Log scrollpane. */
    private javax.swing.JScrollPane logScrollPane;
    /** Tabulka s událostmi simulace. */
    private javax.swing.JTable logTable;
    /** Hlavní tab panel. */
    private javax.swing.JTabbedPane mainTabPane;
    /** Scrollpane pro mapu. */
    private javax.swing.JScrollPane mapScrollPane;
    /** Výplň toolbaru. */
    private javax.swing.Box.Filler middleFiller;
    /** Panel s informacemi o zmrzlinářstvích. */
    private javax.swing.JPanel nodePanel;
    /** Scrollpane pro tabulku zmrzlinářstvími. */
    private javax.swing.JScrollPane nodeScrollPane;
    /** Tabulka zmrzlinářství. */
    private javax.swing.JTable nodeTable;
    /** Tlačítko na odstranění breakpointu. */
    private javax.swing.JButton removeBreakpointButton;
    /** Pevná výplň toolbaru na pravé straně. */
    private javax.swing.Box.Filler rightMarginFiller;
    /** Panel s informacemi o výjezdech. */
    private javax.swing.JPanel routePanel;
    /** Scrollpane pro tabulku výjezdů. */
    private javax.swing.JScrollPane routeScrollPane;
    /** Tabulka výjezdů. */
    private javax.swing.JTable routeTable;
    /** Oddělovač. */
    private javax.swing.JToolBar.Separator separator1;
    /** Oddělovač. */
    private javax.swing.JToolBar.Separator separator2;
    /** Tlačítko pro spuštení simulace. */
    private javax.swing.JButton startSimulationButton;
    /** Popiska stavu simulace. */
    private javax.swing.JLabel statusLabel;
    /** Hlavní toolbar. */
    private javax.swing.JToolBar toolBar;
    /** Tab panel s nástroji. */
    private javax.swing.JTabbedPane toolsTabPane;
    /** Vizualizuace grafu. */
    private cz.zcu.kiv.pt.gui.VisualizationPanel worldMap;
}
