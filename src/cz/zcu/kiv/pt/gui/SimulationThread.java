/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.gui;

import cz.zcu.kiv.pt.simulation.Simulation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Vlákno se simulací.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class SimulationThread extends Thread {
    /** Objekt simulace. */
    private final Simulation simulation;
    
    /** Běží simulace? */
    private boolean running = false;
    
    /** Je simulace pozastavena?  */
    private boolean paused = false;
    
    /** Akce pro zpracování chyb. */
    private ActionListener errorListener;
    
    /** Akce spuštěná po úspěšném ukončení simulace. */
    private ActionListener finishedListener;

    /**
     * Vytvoří vlákno, ve kterém bude spouštěna zadaná simulace.
     * @param sim Objekt simulace.
     */
    public SimulationThread(final Simulation sim) {
        super("SimulationThread");
        this.simulation = sim;
    }

    
    /**
     * Získá instanci {@link ActionListener}u, který naslouchá pro případné chyby.
     * @return {@link ActionListener}, který se vykonává při chybě.
     */
    public ActionListener getErrorListener() {
        return errorListener;
    }

    
    /**
     * Nastaví {@link ActionListener}, který se vykoná při případné chybě.
     * @param newErrorListener Nový {@link ActionListener}.
     */
    public void setErrorListener(final ActionListener newErrorListener) {
        this.errorListener = newErrorListener;
    }
    
    
    /**
     * Nastaví událost, která se vykoná po řádném ukončení simulace.
     * @param newFinishedListener Nový {@link ActionListener}.
     */
    public void setFinishedListener(final ActionListener newFinishedListener) {
        this.finishedListener = newFinishedListener;
    }
    
    
    /**
     * Spustí simulaci.
     */
    @Override
    public void run() {
        try {
            running = true;
            simulation.run();
            if (finishedListener != null) {
                finishedListener.actionPerformed(new ActionEvent(simulation, 0, "finish"));
            }
        } catch (Exception ex) {
            if (errorListener != null) {
                errorListener.actionPerformed(new ActionEvent(ex, 0, "error"));
            }
        } finally {
            running = false;
        }
    }
    
    /**
     * Běží simulace?
     * @return {@code true}, pokud simulace běží.
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Je simulace pozastavena?
     * @return {@code true}, pokud je simulace pozsatavena.
     */
    public boolean isPaused() {
        return paused;
    }

    /**
     * Nastaví příznak pozastavení simulace.
     * @param isPaused Příznak pozastavení.
     */
    public void setPaused(final boolean isPaused) {
        this.paused = isPaused;
    }
    
    /**
     * Získá objekt simulace.
     * @return Simulace.
     */
    public Simulation getSimulation() {
        return simulation;
    }
    
}
