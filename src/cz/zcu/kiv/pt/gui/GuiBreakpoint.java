package cz.zcu.kiv.pt.gui;

import cz.zcu.kiv.pt.simulation.Breakpoint;
import cz.zcu.kiv.pt.simulation.Simulation;
import cz.zcu.kiv.pt.simulation.SimulationTimestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Breakpoint simulace, který pozastaví thread s běžící simulací a nastaví
 * potřebné věci v uživatelském rozhraní.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class GuiBreakpoint extends Breakpoint {
    
    /** Hlavní okno rozhraní. */
    private final MainWindow window;

    /**
     * Vytvoří nový breakpoint.
     * @param sim Objekt simulace.
     * @param breakTime Čas pozastavení.
     * @param mainWindow Hlavní okno GUI.
     */
    public GuiBreakpoint(final Simulation sim, final SimulationTimestamp breakTime, final MainWindow mainWindow) {
        super(sim, breakTime);
        this.window = mainWindow;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void simulationPaused() {
        try {
            window.simulationPaused(this);
            SimulationThread thread = window.getSimulationThread();
            thread.setPaused(true);
            synchronized (thread) {
                while (thread.isPaused()) {
                    thread.wait();
                }
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(GuiBreakpoint.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
