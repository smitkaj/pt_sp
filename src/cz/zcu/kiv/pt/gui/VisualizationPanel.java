/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.gui;

import cz.zcu.kiv.pt.graph.Graph;
import cz.zcu.kiv.pt.graph.GraphNode;
import cz.zcu.kiv.pt.graph.INode;
import cz.zcu.kiv.pt.simulation.Route;
import cz.zcu.kiv.pt.simulation.RouteAction;
import cz.zcu.kiv.pt.simulation.TravelAction;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javax.swing.JPanel;

/**
 * Panel s vizualizací grafu.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class VisualizationPanel extends JPanel {
    /** Serializační ID. */
    private static final long serialVersionUID = 546547987918L;
    
    /** Poloměr zmrzlinářství. */
    private static final int NODE_SIZE = 1;
    
    /** Velikost centrály. */
    private static final int CENTER_SCALE = 2;
    
    /** Poloměr zvýrazněného zmrzlinářství. */
    private static final int HIGHLIGHT_SCALE = 2;
    
    /** Graf. */
    private transient Graph graph;
    
    /** Aktuální výjezd. */
    private transient Route currentRoute;
    
    /** Zvýrazněné zmrzlinářství. */
    private transient INode highlightedNode;
    
    /** Cesty aktuálního výjezdu. */
    private List<Queue<INode>> paths;
    
    /**
     * Nastaví graf.
     * @param newGraph Graf.
     */
    public void setGraph(final Graph newGraph) {
        this.graph = newGraph;
    }
    
    /**
     * Zvýrazní cestu mrazírenského vozu.
     * @param route Výjezd vozu.
     */
    public void highlightRoute(final Route route) {
        this.currentRoute = route;
        prepareRoute();
        this.paint(this.getGraphics());
    }
    
    /**
     * Zvýrazní zmrzlinářství.
     * @param node Pobočka.
     */
    public void highlightNode(final INode node) {
        this.highlightedNode = node;
        this.paint(this.getGraphics());
    }
    
    /**
     * Provede vykreslení vizualizace.
     * @param graphics Plátno.
     */
    @Override
    public void paint(final Graphics graphics) {
        super.paint(graphics);
        
        Graphics2D g2d = (Graphics2D) graphics;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        
        if (graph != null) {
            int size = graph.getSize();
            int worldSize = graph.getWorldSize();
            g2d.setColor(Color.WHITE);
            g2d.fillRect(0, 0, worldSize, worldSize);
            
            g2d.setColor(Color.BLACK);
            drawCenterNode(g2d, (GraphNode) graph.getNode(0));
            GraphNode[] nodes = graph.getNodes();
            for (short i = 1; i < size; i++) {
                drawNode(g2d, nodes[i]);
            }
            
            if (currentRoute != null && paths != null) {
                for (Queue<INode> path : paths) {
                    drawPath(g2d, path, Color.ORANGE);
                }
                
                if (currentRoute.getLastAction() instanceof TravelAction) {
                    drawPath(g2d, ((TravelAction) currentRoute.getLastAction()).getOriginalPath(), Color.RED);
                } else {
                    highlightNode(g2d, currentRoute.getLastAction().getCurrentNode());
                }
            }
            
            if (highlightedNode != null) {
                g2d.setColor(Color.ORANGE);
                g2d.drawLine(0, highlightedNode.getY(), worldSize, highlightedNode.getY());
                g2d.drawLine(highlightedNode.getX(), 0, highlightedNode.getX(), worldSize);
                highlightNode(g2d, highlightedNode);
            }
        }
    }
    
    /**
     * Vykreslí centrálu.
     * @param graphics Plátno.
     * @param node Centrála.
     */
    private void drawCenterNode(final Graphics2D graphics, final INode node) {
        graphics.fillRect(
            node.getX() - CENTER_SCALE * NODE_SIZE,
            node.getY() - CENTER_SCALE * NODE_SIZE,
            CENTER_SCALE * 2 * NODE_SIZE + 1,
            CENTER_SCALE * 2 * NODE_SIZE + 1
        );
    }
    
    /**
     * Vykreslí zmrzlinářství.
     * @param graphics Plátno.
     * @param node Pobočka.
     */
    private void drawNode(final Graphics2D graphics, final INode node) {
        graphics.drawOval(
            node.getX() - NODE_SIZE,
            node.getY() - NODE_SIZE,
            2 * NODE_SIZE + 1,
            2 * NODE_SIZE + 1
        );
    }
    
    
    /**
     * Zvýrazní zmrzlinářství.
     * @param graphics Plátno.
     * @param node Pobočka.
     */
    private void highlightNode(final Graphics2D graphics, final INode node) {
        graphics.setColor(Color.red);
        graphics.fillOval(
            node.getX() - HIGHLIGHT_SCALE * NODE_SIZE,
            node.getY() - HIGHLIGHT_SCALE * NODE_SIZE,
            HIGHLIGHT_SCALE * 2 * NODE_SIZE + 1,
            HIGHLIGHT_SCALE * 2 * NODE_SIZE + 1
        );
    }
    
    
    /**
     * Vykreslí cestu.
     * @param graphics Plátno.
     * @param path Cesta.
     * @param color Barva cesty.
     */
    private void drawPath(final Graphics2D graphics, final Queue<INode> path, final Color color) {
        graphics.setColor(color);
        
        Iterator<INode> itr = path.iterator();
        INode current = itr.next(), next;
        while (itr.hasNext()) {
            next = itr.next();
            graphics.drawLine(current.getX(), current.getY(), next.getX(), next.getY());
            current = next;
        }
    }
    
    /**
     * Interní metoda na vypreparování kompletní cesty vozu.
     */
    private void prepareRoute() {
        paths = new LinkedList<Queue<INode>>();
        
        for (RouteAction routeAction : currentRoute.getScheudle().getActions()) {
            if (routeAction instanceof TravelAction) {
                paths.add(((TravelAction) routeAction).getOriginalPath());
            }
        }
    }
    
}
