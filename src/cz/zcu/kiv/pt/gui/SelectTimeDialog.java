/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package cz.zcu.kiv.pt.gui;


import cz.zcu.kiv.pt.simulation.SimulationTimestamp;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.HOUR;
import static cz.zcu.kiv.pt.simulation.SimulationTimestamp.MINUTE;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

/**
 * Dialog pro výběr času.
 * @author Barbora Janská
 * @author Jan Smitka
 */
public final class SelectTimeDialog extends javax.swing.JDialog {
    /** Serializační ID. */
    private static final long serialVersionUID = 89654L;
    
    /** Potvrdil uživatel čas? */
    private boolean result = false;
    
    /** Zadaný čas. */
    private transient SimulationTimestamp time;

    /**
     * Vytvoří nový dialog pro výběr času.
     * @param parent Rodičovský formulář.
     */
    public SelectTimeDialog(final java.awt.Frame parent) {
        super(parent, true);
        initComponents();
        rootPane.setDefaultButton(okButton);
        setLocationRelativeTo(parent);
    }

    
    /**
     * Vytvoří formatter pro políčko na zadání času.
     * @return Nový formatter.
     * @throws ParseException V případě, že je uveden neplatný formátovací řetězec. K tomu nikdy nedojde, protože je uveden přímo v kódu.
     */
    private JFormattedTextField.AbstractFormatterFactory createFormatter() throws ParseException {
        MaskFormatter formatter = new MaskFormatter("#. den ##:##:##");
        formatter.setPlaceholder("1. den 00:00:00");
        return new DefaultFormatterFactory(formatter);
    }
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        timeField = new javax.swing.JFormattedTextField();
        timeLabel = new javax.swing.JLabel();
        okButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Výběr času");
        setResizable(false);

        try {
            timeField.setFormatterFactory(createFormatter());
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        timeLabel.setText("Čas:");

        okButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cz/zcu/kiv/pt/gui/resources/tick.png"))); // NOI18N
        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(final java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(timeLabel)
                        .addGap(18, 18, 18)
                        .addComponent(timeField, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 96, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(okButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(timeField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timeLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addComponent(okButton)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>

    
    /**
     * Událost po stisknutí tlačítka OK. Zajistí naparsování zadaného času.
     * @param evt Událost.
     */
    private void okButtonActionPerformed(final java.awt.event.ActionEvent evt) {
        Pattern pattern = Pattern.compile("^(\\d+).+?(\\d{1,2}):(\\d{2}):(\\d{2})$");
        Matcher matcher = pattern.matcher(timeField.getText());
        if (matcher.matches()) {
            byte day = Byte.valueOf(matcher.group(1));
            byte hours = Byte.valueOf(matcher.group(2));
            byte minutes = Byte.valueOf(matcher.group(3));
            byte seconds = Byte.valueOf(matcher.group(4));
            time = new SimulationTimestamp(day, hours * HOUR + minutes * MINUTE + seconds);
            result = true;
            setVisible(false);
        } else {
            JOptionPane.showMessageDialog(this, "Neplatný čas simulace.");
        }
    }

    
    /**
     * Zjistí, zda byla akce okna úspěšná, tedy zda by zadán správný čas.
     * @return Byl zadán správný čas?
     */
    public boolean getResult() {
        return result;
    }

    
    /**
     * Získá uživatelem zadaný čas.
     * @return Zadaný čas.
     */
    public SimulationTimestamp getTime() {
        return time;
    }
    

    /** Potvrzovací tlačítko. */
    private javax.swing.JButton okButton;
    /** Políčko na zadání času. */
    private javax.swing.JFormattedTextField timeField;
    /** Popiska k políčku na čas. */
    private javax.swing.JLabel timeLabel;
}
