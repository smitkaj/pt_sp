/**
 * Uživatelské rozhraní aplikace. Obsahuje jedno hlavní okno a několik dalších
 * pomocných dialogů. Zajišťuje také běh simulace v odděleném vlákně a jeho
 * ovládání.
 */
package cz.zcu.kiv.pt.gui;